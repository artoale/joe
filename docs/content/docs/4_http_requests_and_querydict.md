---
date: '2024-11-11T21:25:16+01:00'
draft: false
title: 'Chapter 4: QueryDict and HttpRequests'
linkTitle: 'QueryDict and HttpRequests'
breadcrumbs: false
weight: 4
---

You may remember from Chapter 1 that our initial implementation of HttpRequest was
doing almost nothing apart from storing the request method and path - which is enough to prove the concept, but not enough to build even the most basic of web application.

Some of the things programmers need when writing a view:

1. Access query parameters from the path (things like ?this=that&cool=joe)
2. Accessing data that is attached to a request (e.g. POST to a form, body of an API call, etc..)
3. Checking request header

Django does this by providing a few attributes on the request object, in particular `HttpRequest.GET` for query parameter access, `HttpRequest.POST` for
POST data, `HttpRequest.FILES` for uploaded files and `HttpRequest.headers` for Http headers - these are all dictionary-like object augmented with some extra capabilities, like parsing querystrings, forcing immutability or allowing case-insensitive operations.

## GET and POST: QueryDict

In Django, those dictionary-like objects are called `QueryDict`, which are an extension of dictionaries that make it nicer to deal with list values, and have query-parsing capabilities.

Here's the subset of features we'd like to support in Joe:

```python {filename="Python"}
>>> get_data = QueryDict('a=1&a=2&c=3')
>>> get_data
<QueryDict: {'a': ['1', '2'], 'c': ['3']}>

>>> get_data['a']
'2'

>>> get_data.getlist('a')
['1', '2']

>>> get_data['d'] = '4'
>>> get_data
<QueryDict: {'a': ['1', '2'], 'c': ['3'], 'd': ['4']}>

>>> get_data.setlist('e', ['5'])
>>> get_data
<QueryDict: {'a': ['1', '2'], 'c': ['3'], 'd': ['4'], 'e': ['5']}>

>>> get_data.appendlist('a', '21')
>>> get_data
<QueryDict: {'a': ['1', '2', '21'], 'c': ['3'], 'd': ['4'], 'e': ['5']}>

>>> [x for x in get_data.items()]
[('a', '21'), ('c', '3'), ('d', '4'), ('e', '5')]

>>> [l for l in get_data.lists()]
[('a', ['1', '2', '21']), ('c', ['3']), ('d', ['4']), ('e', ['5'])]
```

As you can see, when dealing with single values, it behaves just like a `dict`, but if more than one value is provided (which is the case, for example, when you have a multiple-choice field in a form) then you can use
the `getlist`, `setlist`, `appendlist` and `lists` methods.

{{< callout type="info" emoji="💭" >}}
I've never been fully convinced by this API, since it lulls you into thinking it is a dictionary, but it sometimes behave differently. I decided to follow the same approach as Django because it's useful to see how to implement the dict data model and the various special methods associated with subscripting.
{{< /callout >}}

The implementation is not very long, we'll add it to `joe.request` and look at it in detail.

```python {filename="joe/request.py"}
class QueryDict(UserDict):
    def __init__(self, query_string=None):
        # Assuming query_string is already decoded here

        super().__init__()

        if query_string:
            query_data = parse_qs(
                query_string,
                keep_blank_values=True,
                max_num_fields=settings.DATA_UPLOAD_MAX_NUMBER_FIELDS,
            )
            self.data.update(query_data)

    def __getitem__(self, key):
        if key not in self.data:
            raise KeyError(key)

        try:
            return self.data[key][-1]
        except IndexError:
            return []

    def __setitem__(self, key, item):
        self.data[key] = [item]

    def getlist(self, key):
        """Return the list associated with key."""
        return self.data[key]

    def setlist(self, key, value):
        """Replace key with the provided list"""
        self.data[key] = value

    def appendlist(self, key, value):
        """Add value to the list key, creating it if not defined"""
        self.data.setdefault(key, [])
        self.data[key].append(value)

    def items(self):
        """Iterator that respects __getitem__ semantics

        Only returns last item of lists
        """
        return ((key, self[key]) for key in self.data.keys())

    def lists(self):
        """Iterator for (key, list) tuples"""
        return self.data.items()

    def __repr__(self):
        return f"<QueryDict: {self.data!r}>"
```
First, note the class we're extending from: `UserDict`.

When creating custom dictionaries, developers have two options, extend `dict` or `collections.UserDict`. Historically, extending `dict` wasn't possible and UserDict was introduced to the standard library as a workaround. Now that it is possible, the main differences between the two options are that:
* the `dict` implementation doesn't call `__setitem__` in other methods (like `__init__`, or `update`)
* for `UserDict` the "underlying" dictionary is stored in a `data` attribute. This tends to make your code a bit easier to understand, but potentially a fraction slower (since there's an extra layer the interpreter has to go through every time).

In this implementation, we've used `UserDict` for clarity.

Looking at the `__init__`, we defer to `urllib.parse.parse_qs` to do the heavy lifting for us and parse the query string. Note that we're passing
a value to `max_num_fields` that can be configured via settings. Parsing query strings can become time expensive with hilariously large numbers of fields - and potentially lead to DoS attacks - so we set a default of 1000, which should be plenty for most application, and could be configured to larger values if a legitimate need arises.

### Supporting subscript in your own classes

We now move to the interesting parts. `__getitem__` and `__setitem__`. These are the method defined by python data model when accessing and assigning to an object using subscript notation (square brackets, like `x['a']`).
These are the two essential methods you need to implement to support that syntax in your classes in python.

`UserDict` provides a default implementation that simply defers to the internal dictionary, but in our implementation, we want to override them to operate on lists.

`__getitem__` is called when someone wants to `get` a value e.g. `QueryDict['some_value']` and, as you may guess, it receive that value as parameter.
In our implementation we return the item inside the "internal" list - not the list itself - but if there are multiple values, the last one is returned. In case the list is empty, we return an empty list instead of `None` following Django's convention.

`__setitem__` provides the mirror `set` implementation, wrapping the item in a list before storing it.

We also provides a few utility methods (`getlist`, `setlist`, `appendlist`) to allow us to work on the raw list when we need to.

Finally, we override `UserDict.items()` to behave consistently with `__getitem__`, so that only one item in each list is iterated over. To support iterating over all lists, we provide the `QueryDict.lists()` method instead.

## A more useful HttpRequest class

With a working QueryDict, we can make the HttpRequest class a bit more useful.

We'll need to change the `__init__` method to take a few extra params and save them on the instance:

```python {filename="joe/request.py"}
class HttpRequest:
    def __init__(
        self,
        method: str,
        path: str,
        headers, # Provided by BaseHTTPRequest as a case-insensitive dict-like object
        query_string: str, # This will also come from the server handler
        stream: BinaryIO, # the file-like read object, already opened
    ):
        super().__init__()
        self.method = method
        self.path = path
        self.headers = headers

        try:
            self.encoding = headers.get_charsets()[0] or "utf-8"
        except IndexError:
            self.encoding = "utf-8"

        self._stream = stream
        self._query_string = query_string
```

Now we need to add the `GET` and `POST` dictionaries. While we could just
add them to the `__init__`, we'll try to be a bit more conscious.
Since those operation are potentially expensive and not necessarily needed by all
views, we'll try to keep them lazy.

For this, we'll use the `functool.cached_property` decorator, which allows us to turn any parameter-less method into a property that is computed once (when first used), and then cached on the instance:

```python {filename="joe/request.py"}
class HttpRequest:
    ...
    @cached_property
    def GET(self):
        return QueryDict(self._query_string)

    @cached_property
    def POST(self):
        if self.headers["Content-Type"] == "multipart/form-data":
            raise NotImplementedError("multipart/form-data")

        if self.headers["Content-Type"] == "application/x-www-form-urlencoded":
            query = self.body.decode(self.encoding)
            return QueryDict(query)

        return QueryDict()
```

The code above expect a `body` attribute, which we haven't defined yet.
We want to read the input stream (as long as it's not larger than some configurable number) cache it,
and return the raw bytes.

```python {filename="joe/request.py"}
    ...
    @cached_property
    def body(self):
        try:
            to_read = int(self.headers["Content-Length"])
        except ValueError:
            to_read = 0

        if to_read > settings.DATA_UPLOAD_MAX_MEMORY_SIZE:
            raise OSError(f"Request body too large ({to_read} bytes).")

        # Let this raise
        return self._stream.read(to_read)
```
We'll also add the `DATA_UPLOAD_MAX_MEMORY_SIZE` settings to our global, using Django's default of 2.5MiB. This is to avoid having a malicious user overwhelm our server
by uploading a gigantic amount of data.

## Server updates

One step left: updating the server to provide the necessary arguments.

Let's update the `_handle_request` method to do just that:

```python {filename="joe/server.py"}
from urllib.parse import urlparse

class RequestHandler(BaseHTTPRequestHandler):
    ...
     def _handle_request(self, verb: str):
        url = urlparse(self.path)
        request = HttpRequest(
            verb,
            url.path,
            self.headers,
            query_string=url.query,
            stream=self.rfile,
        )

        response = self._dispatcher.dispatch(request)
        ...
        # Rest of the code left unchanged
```

We're using the standard lib `urllib.parse` module again, this time to parse the full path and split the "proper" path from the query string, and pass along the remaining arguments.
`self.headers` is an instance of `Message`, a class intended for email handling which provides an API similar enough to Django's so we won't need to implement that 🙂

## Testing it all out

We'll change our `home` view to send a message in italian if the `lang` query parameter has the value `it`.
We'll also add some logging to the `details` view to log the POST data

```python {filename="example_app/views.py"}
def home(req):
    logger.info("Home called")

    if req.GET.get("lang") == "it":
        return HttpResponse("Benvenuti nell'app di esempio!")

    return HttpResponse("Welcome to the example app!")


def details(req, details):
    if req.method == "POST":
        logger.info("POST received")
        logger.info("DATA %r", req.POST)
    else:
        logger.info("Details called")

    return HttpResponse(f"Details: {details}")
```

We can now test our app by running `python example_app/manage.py` and checking
the italian variation at `http://localhost:8080/?lang=it`.

## Recap

We updated the HttpRequest to provide headers, GET and POST data which are now
available for our views to use. A further improvement could be to add support
for `multipart/form-data` (hint: the `email` module in the standard library would
be a good place to start) - but that's left as an exercise to the reader 🙃.

Along the way, we've learned about `@cached_property`, as well as how to support subscript notation for user-defined classes with `__getitem__` and `__setitem__`.

In the next chapter we'll look at another extremely useful feature that virtually
any web framework provides, and how to implement it in Joe: middlewares.
