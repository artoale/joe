---
date: '2024-11-11T21:25:16+01:00'
draft: false
title: 'Chapter 5: Middlewares'
linkTitle: 'Middlewares'
breadcrumbs: false
weight: 5
---

Most web frameworks provide developers with a way to write code that intercepts either requests,
responses, or both so that they can add functionality shared by all views. Django is no exception and it provides the ability to tweak the
request/response cycle via the concept of "middlewares".

Before trying to implement it, let's see how it works.

## The process
There are 3 main steps involved in the process:

{{% steps %}}

### Process the request

When Django receives a request, before routing it to the right view, it goes through a series
of user-provided functions that can alter it, by adding or tweaking attributes.
For example, the `SessionMiddleware` checks the cookies sent by the browser and retrieves the
session data associated with it. It then makes it available to views via the `request.session` attribute.

### Call the view

The view itself is called (if one exists) or a 404 error is raised. In any case, an `HttpResponse` object is created.

### Alter the response

The response object is then passed through functions that can alter it in a similar way as
during the request phase. In the case of `SessionMiddleware`, at this stage the response object
would be modified to include the necessary cookie headers.

{{% /steps %}}

In addition to the basic behaviour, Django also provides other hooks for things like
custom exception handling, view-aware middlewares and templating. We'll focus on the
3 core steps in Joe.

## Django's Middleware API

Since Django 1.10 the API for writing a middleware has been significantly streamlined.
Let's have a look at it!

Django expects a `MIDDLEWARE` settings to be a list of Python paths (as strings) to "middleware factories". Looking at the code first might be the easiest way to see what a factory is in this context

```python {filename="Python"}
def middleware_factory(get_response):cd
    # Initial setup if necessary (one off)

    def middleware(request):
        # Optionally tweak the request (step 1)
        response = get_response(request)  # Call the view (step 2)
        # Optionally tweak the response (step 3)
        return response

    return middleware
```

The `middleware_factory` should return a function which is the middleware instance.
This will receive request, optionally change them and then delegate to `get_response`.

It's important to note that `get_response` is "the next handler", which could be either another middleware or the actual view.

The middleware instance then has a chance to tweak the response if it wants
to, it returns it "up" the chain (to either the middleware that comes before in the list, or to django itself, who will dispatch it to the browser).

{{< callout type="warning" >}}
Note that each middleware is "trusted" to call the next one in the chain via `get_response` and
return its response, but it doesn't have to - it could return without calling `get_response` at all,
raise an exception or completely replace the whole response object.
{{< /callout >}}

## Take one: per-request setup

In Chapter 2 we created a class responsible for handling request and calling the right view:
the `Dispatcher`. We can start by adding some functionality to it, to explore how middlewares in
joe could be implemented.

First, we rename the `dispatch` method to a private `_do_dispatch` one, so that the second step, "calling the view", is encapsulated in a single method which we can call at the right time.

```python {filename="joe/dispatcher.py"}
class Dispatcher:
    ...
    def _do_dispatch(self, request):
        path = request.path

        resolved = self.root_resolver.resolve(path)

        if not resolved:
            return HttpResponse("Page not found", status_code=404)

        view, kwargs = resolved

        try:
            return view(request, **kwargs)
        except Exception as e:
            logger.error("Internal Server Error: %s", e, exc_info=True)
            return HttpResponse("Internal server error", status_code=500)
    ...
```

We then add a few example middleware just to test things out and we implement a new `dispatch`
method that calls the factory in the right order.


```python {filename="joe/dispatcher.py"}
def middleware_factory_1(get_response):
    def middleware(request):
        return get_response(request)
    return middleware

def middleware_factory_2(get_response):
    def middleware(request):
        return get_response(request)
    return middleware

middleware_factories = [
    middleware_factory_1,
    middleware_factory_2,
]

class Dispatcher:
    ...

    def dispatch(self, request):
        get_next = self._do_dispatch

        for middleware_factory in reversed(middleware_factories):
            middleware = middleware_factory(get_next)
            get_next = middleware

        return get_next(request)
```

The `dispatch` method is really the core of the solution here, so let's see how it works in details.

We want the chain of middleware to behave like this:

```
dispatch
    ↳ calls middleware_1
        ↳ calls middleware_2
            ↳ calls _do_dispatch
                ↳ calls the actual view
                    view returns response to _do_dispatch ↵
                returns response to middleware_2 ↵
            returns response to middleware_1 ↵
        returns response to dispatch ↵
    returns response to caller (RequestHandler)
```

It's useful to think about this as an "onion" of nested calls: we need to start from the "core"
(the `_do_dispatch` method) and build up the outer layers.
This is because each layer needs to receive the inner layer (the `get_response` parameter) when it is called.

We use the local `get_next` variable to keep track of the top layer built so far. At the beginning,
the outer layer is also the innermost one, so we start from the `_do_dispatch` method.

We then iterate on the list of `middleware_factories` in **reverse order**, passing the inner layer
which we instantiated in the previous iteration.

After the last iteration, `get_next` will point to the outermost layer (the first middleware in the
chain). We can call that to trigger the chain to be executed.

## Take two: one-off instantiation and module loading

While the implementation in the first step works, it doesn't really respect the behaviour we described
before, mainly for two reasons:

* The factory function is not a "one off" call, but it gets called for every request
* We are not using the `MIDDLEWARE` setting as Django does

With the basic logic covered, fixing these issues is fairly easy.

First, we move the logic for setting up the middleware chain to its own method, which we'll call in
the dispatcher `__init__`:

```python {filename="joe/dispatcher.py"}
class Dispatcher:
    def __init__(self) -> None:
        self.root_resolver = URLResolver(r"^/", include(settings.ROOT_URLCONF))
        self._setup_middlewares()

    def _setup_middlewares(self) -> None:
        """Creates the middleware chain"""

        get_next = self._do_dispatch

        for middleware_path in reversed(settings.MIDDLEWARE):
            module_path, module_prop = middleware_path.rsplit(".", 1)
            middleware_module = import_module(module_path)
            middleware = getattr(middleware_module, module_prop)

            get_next = middleware(get_next)

            if not callable(get_next):
                raise ImproperlyConfigured(
                    f"Middleware initialization '{middleware_path}' returned {get_next}"
                    " which is not a valid middleware (callable expected)."
                )

        self._middleware_chain = previous

    ...
```

We're using `import_module` as we've done in previous chapters to load
the correct module based on the dotted path.

Note: since we're expecting the path string to point to the function itself
(e.g. `example_app.middlewares.session_middleware`) we need to do a bit
of string manipulation to separate the module bit (e.g. `example_app.middlewares`) from the middleware factory itself (e.g. `session_middleware`)

Finally, we store the middleware chain on the instance for later use when
`dispatch` is called by the `RequestHandler`.

Here's how we'll use it:

```python {filename="joe/dispatcher.py"}
    def dispatch(self, request):
        try:
            return self._middleware_chain(request)
        except Exception as e:
            logger.error("Internal Server Error: %s", e, exc_info=True)
            return HttpResponse("Internal server error", status_code=500)
```

## Take three: improving error handling

Anyone who likes the DRY principle would probably find this repetition
in error handling a bit suspicious, so let's refactor that out.

We're catching exceptions raised by a function call so that we instead return
a 500 HttpResponses. This lends itself well to abstraction by using a wrapper function.

```python {filename="joe/dispatcher.py"}
from functools import wraps

def exception_to_response(get_response):

    @wraps(get_response)
    def inner(request):
        try:
            return get_response(request)
        except Exception:
            return HttpResponse("Internal server error", status_code=500)
    return inner
```

We can now use the `exception_to_response` function to wrap any handler that
might raise an exception and turn it into a useful HttpResponse. Django's
implementation is refined since it provides custom exception for different type of HTTP error that client code can raise. Django then automatically turns those exceptions
into their respective HttpResponse (e.g. raising an `Http403` exception will result in an `HttpResponse(status_code=403)` being returned to the client).

The code itself is fairly straightforward except maybe for the use of the `functools.wraps` decorator.

While strictly speaking not necessary, it's quite useful for debugging, or whenever you need better metadata about functions you're wrapping: metadata is copied from the wrapped function, in this case `get_response`, to the wrapper (`inner`) so that, for example, if you have:

```python
def do_something(get_response)
    """Does something"""
    ...

wrapped = exception_to_response(do_something)
print(wrapped, wrapped.__doc__)
```
it would output something like `<function do_something at 0x10b7e79d0> Does something`,
which is much more informative than the `<function exception_to_response.<locals>.inner at 0x10bc86dc0> None` which you'd get without the `@wraps` decorator.

Whenever you're wrapping a function to tweak its behaviour (typically, when
you're writing decorators) remember to use `functools.wraps`, your future self will thank you!

We can now update our Dispatcher to be DRYer, here's the full code:

```python {filename="joe/dispatcher.py"}

class Dispatcher:
    def __init__(self):
        self.root_resolver = URLResolver(r"^/", include(settings.ROOT_URLCONF))
        self._setup_middlewares()

    def _setup_middlewares(self):
        """Creates the middleware chain"""

        get_next = exception_to_response(self._do_dispatch)

        for middleware_path in reversed(settings.MIDDLEWARE):
            module_path, module_prop = middleware_path.rsplit(".", 1)
            middleware_module = import_module(module_path)
            middleware = getattr(middleware_module, module_prop)

            get_next = middleware(get_next)
            if not callable(get_next):
                raise ImproperlyConfigured(
                    f"Middleware initialization '{middleware_path}' returned {get_next}"
                    " which is not a valid middleware (callable expected)."
                )

            get_next = exception_to_response(get_next)

        self._middleware_chain = get_next

    def _do_dispatch(self, request):
        path = request.path
        response = None
        logger.debug("Got request for path %r", path)

        resolved = self.root_resolver.resolve(path)

        if not resolved:
            logger.debug("No route found for match")
            return HttpResponse("Page not found", status_code=404)

        view, kwargs = resolved
        for view_processor in self._process_views_middlewares:
            response = view_processor(request, view, kwargs)
            if response:
                return response

        return view(request, **kwargs)

    def dispatch(self, request):
        return self._middleware_chain(request)

```

We can try it by adding a couple of entries in the `MIDDLEWARE` setting in the example app and run the server.

## Json parsing with a middleware

One thing a middleware can be quite useful is parsing json content
in requests. We can experiment with that in our example app.

Let's add this to a new `example_app/middlewares.py`:

```python {filename="example_app/middlewares.py"}
import json
import logging

logger = logging.getLogger(__name__)


def json_decoder(get_response):
    """
    Middleware for decoding json-encoded requests
    """

    def middleware(request):
        if request.path.startswith("/api/") and request.method in (
            "POST",
            "PUT",
            "PATCH",
        ):
            try:
                body = request.body.decode(request.encoding)
                json_payload = json.loads(body)
                request.json = json_payload
            except json.JSONDecodeError:
                logger.warning("Failed to parse json body")

        return get_response(request)

    return middleware
```

We then configure a new view...

```python {filename="example_app/urls.py"}
def api(req):
    """
    View that shows how a middleware can provide json parsing
    """
    if req.method == "POST":
        logger.info("API POST received with %r", req.json)
        return HttpResponse('{"status": "ok"}', content_type="application/json")
    return HttpResponse('{"status": "meh"}', content_type="application/json")
```

...and its URL config:

```python {filename="example_app/urls.py"}
urlpatterns = [
    # ...
    re_path(r"^api/$", views.api),
    # ...
]
```

As you can see, the middleware enhance the request object by adding a json
attribute for a subset of URLs. This is a very common pattern in Django
and worth remembering when writing your own middlewares.

## Middlewares as classes

While the "function" format is the simpler and easier to reason about, it's sometimes
useful to write the logic in a class rather than a function, for example to make use of
inheritance.
Python provides a really powerful way to do that without having to change our code at all,  thanks to the `__call__` method.

When `middleware_factory` is called, if instead of a function our user provide us with a class,
the python interpreter will happily instantiate it (in some languages a constructor
is very different from a regular function, so this wouldn't be possible).

We then have an instance of our class ready for usage, but how do we make it "callable"? Simple, by
implementing the `__call__` dunder method. When a function invocation is applied to an object, the
interpreter checks if the object has the `__call__` attribute. If it does, it calls it with the same argument it would a plain function.

Here's how we could write the first example of this chapter
in class form:

```python {filename="Python"}
class MyMiddleware:
    def __init__(self, get_response):
        # Initial setup if necessary (one off)
        self.get_response = get_response

    def __call__(self, request):
        # Optionally tweak the request (step 1)
        response = get_response(request)  # Call the view (step 2)
        # Optionally tweak the response (step 3)
        return response

```

## Recap

We added support for middlewares, providing a powerful way to hook into joe's request/response cycle, and we learned about some new python features (`functools.wrap` and `__call__`).

If you're curious, in the companion code, we also provided code for supporting the `process_view` and `process_exception` hooks which Django provided respectively to allow middleware to behave differently depending on the resolved view (for example a login middleware may restrict access to all views, except for the "homepage" and "login" views) - and to provide custom handling of exception that may be raised by the view.
These extra features are easy to add so we won't look at them in details here.

We have covered a lot of ground when it comes to handling the HTTP request/response cycle, but Django (unlike other frameworks) provides a lot more!

In the next chapter we'll start looking at one of the biggest and more complex parts of Django, the model layer, which will also give us the opportunity to study more advanced meta programming techniques.
