---
date: '2024-11-11T21:25:16+01:00'
draft: false
linkTitle: 'Add support for settings'
title: 'Chapter 3: Add support for settings'
breadcrumbs: false
weight: 3
---

In the previous chapter we set up basic handling of HTTP requests as well as provided a way for
application code to actually write its own views and map them to views, but we had to hardcode
those values in joe.
In this chapter we'll fix this by allowing client application to define a settings module in a few different ways.

The main way to configure django's settings module is via the environment variable `DJANGO_SETTINGS_MODULE` (or, equivalently, via the --settings argument to `manage.py`/`django-admin`).

A basic implementation that emulates that behaviour follows.

## The `Settings` class

Let's first see how the setting object is typically used in Django:

```python {filename="Python"}
from joe.conf import settings

def a_function():
    if settings.DEBUG:
        show_debug_info()
    ...
```
Notice that we're importing a `joe` module, `joe.conf`, but we expect that to be
populated with our own configuration

There's a couple of ways this could be implemented:
* as a module with dynamically generated attributes (e.g. using `__getattr__` at module level)
* with `settings` as the instance of a class instantiated at module level.

We'll go for the second approach.

{{< callout type="info" emoji="📜" >}}
_Historical note:_ In very old versions of Django (<0.95.x) the settings module
was implemented as a dynamic module. It was later changed to the global object implementation.
{{< /callout >}}

In our implementation we also have a couple of rules we want to keep in mind:

* Only uppercase identifiers are considered a settings value
* We want to provide default values for some of those settings

Let's add a basic default setting files. We can create a new package `joe.conf` with a `global_settings.py` module in it

```python {filename="joe/conf/global_settings.py"}
DEBUG = False
DATABASES = {}
```
This only contains some fairly arbitrary settings, common to Django apps.

In the `joe/db/__init__.py` file we'll instead add the code proper:

```python {filename="joe/conf/__init__.py"}
import os

from importlib import import_module
from joe.conf import global_settings

class Settings:
    def __init__(self, settings_module):
        # Load the module first, so that it blows quickly if misconfigured
        module = import_module(settings_module)

        # Load default settings
        for key in dir(global_settings):
            if key.isupper():
                setattr(self, key, getattr(global_settings, key))

        # Then user-defined settings
        for key in dir(module):
            if key.isupper():
                value = getattr(module, key)
                # We could do some validation and checks here - e.g. warn if a given setting is in
                # the wrong format
                setattr(self, key, value)
```

The `__init__` method does all the work here: it imports the module dynamically
(again, using `importlib.import_module` as we've seen in Chapter 2). It then
sets attributes on itself based on all the (uppercase) default settings as well
as the user configured ones.

We can then instantiate that class in the same module based on the environment variable, like this:

```python {filename="joe/conf/__init__.py"}
# ...
ENV_VARIABLE = 'JOE_SETTINGS_MODULE'

SETTINGS_MODULE_NAME = os.environ.get(ENV_VARIABLE)
if not SETTINGS_MODULE_NAME:
    raise RuntimeError(f"{ENV_VARIABLE} not configured")

settings = Settings(SETTINGS_MODULE_NAME)
```

We're providing access to settings via a global instance of the Settings class made available in the conf module. This is a very common design pattern in the python community, usually referred to as the "Global Object Pattern". It serves a similar purpose as the "Singleton" pattern, and you can find a good explanation of the differences in the brilliant [Python Patterns](https://python-patterns.guide/gang-of-four/singleton/) website.

### Aside: Django and LazySettings

Our settings implementation is very minimal compared to Django's own in many aspects, but one of them is worth calling out - the usage of `LazyObject`.

Django provides many useful modules, that could be used standalone in a non-django application. In those cases, having an environment variable and config file for Django may be undesirable, so django allows user to configure settings manually via `django.conf.settings.configure()`. This requires settings not to be set when the settings module is imported, but only when it is actually accessed, to give time to the call to `settings.configure()` to have happened.

To support this behaviour, django provides a generic `LazyObject` class that can be used to act as wrapper to instantiate another class (in this case `Settings`) only when one of its attribute is accessed. While its implementation is not overly complicated it has to cover many edge cases, so we'll leave it out for now.

### Updating Joe to use external apps settings

In Chapter 2, we hardcoded the external app root url module in the dispatcher.
We can now use the new settings mechanism to fix that.
First, we create `example_app/settings.py`  with a single setting:

```python {filename="example_app/settings.py"}
ROOT_URLCONF = "example_app.urls"
```

Then we update the `Dispatcher` class to use the setting value instead of the hardcoded one in `dispatcher.py`

```python {filename="joe/dispatcher.py"}
from joe.conf import settings
...

class Dispatcher:
    def __init__(self) -> None:
        self.root_resolver = URLResolver(r"^/", include(settings.ROOT_URLCONF))

    def dispatch(self):
        ...
```

That's it! We can then run joe with a "configured" external app with
```sh
JOE_SETTINGS_MODULE='example_app.settings' python -m joe
```

### More about lazy behaviour in Django

If you try to remove the ROOT_URLCONF setting from your example app, you'll see joe fails to start with a `AttributeError: 'Settings' object has no attribute 'ROOT_URLCONF'`. If you try this on a Django app, this won't happen and `runserver` will happily work - that is, until you make a request to your website, at which point you'll see a very similar attribute error. This has a very lazy approach to instantiation, and wherever it can it creates object only when needed and then cache them if necessary. This is sometimes a performance optimisation, sometimes a necessity, like we discussed before when talking about `LazyObject`. In `joe` we'll try to avoid that extra complexity when it's not necessary, but it is a very useful pattern to be aware of.

## Argument parsing and manage.py

Setting an env variable is fine, and it's probably a good enough solution for
when you're running the server, but it makes for a slightly annoying syntax to use in the terminal. It'd be nicer to be able to do this:

```sh
python -m joe --settings=example_app.settings
```

even better, we'd like to be able to do, as with django:

```sh
cd example_app
python ./manage.py
```

There's a couple of changes we need to do to support that.
First we need to do a small refactor to encapsulate our logic outside
the `joe/__main__.py`, so it can be reused - and since we're there we'll throw
in some cli argument parsing for good measure.

We'll then use these to create `example_app/manage.py`

### Reusable initialization code

As per python guidelines, the `__main__.py` files should be minimal and simply call another function defined elsewhere. This allows for a more reusable solution, since that function can be then called from other code (as we'd like to do in `manage.py`).

So let's replace  `joe/__main__.py` with a super simple module that import and executes a function:

```python {filename="joe/__main__.py"}
from joe.management import execute_from_command_line

execute_from_command_line()
```

Can't get much simpler than that!

Now, let's create the function we've just pretended we had in `joe/management.py`:

```python {filename="joe/management.py"}
import argparse
import logging
import os
import sys

host = "localhost"
port = 8080

def get_parser():
    prog_name = os.path.basename(sys.argv[0])

    if prog_name == "__main__.py":
        prog_name = "python -m joe"

    parser = argparse.ArgumentParser(prog=prog_name)

    parser.add_argument(
        "--settings",
        action="store",
        help="Sets the settings module.",
        default=None,
    )

    return parser


def execute_from_command_line(argv=None):
    argv = argv if argv is not None else sys.argv[1:]
    parser = get_parser()
    args = parser.parse_args(argv)

    if args.settings:
        os.environ["JOE_SETTINGS_MODULE"] = args.settings

    # Since our settings are not lazy, import needs to happen here
    from joe.server import get_server

    logger.info("Starting http server")
    server = get_server(host, port)
    try:
        logger.info("Listening on %s:%d", host, port)
        server.serve_forever()
    except KeyboardInterrupt:
        logger.info("Received Keyboard interrupt, shutting down")
    except OSError as e:
        logger.error("Error while creating HTTP server %s", e)

    exit(1)
```

First, we use the excellent `argparse` from the standard lib to allow for a `--settings` option in the cli, which we then use in our `execute_from_command_line` function.

{{< callout type="info" >}}
At this stage, Django would parse the command (e.g. `runserver`, `test`, `makemessages`, `makemigrations`, etc..). Since we only have one we'll just start the server immediately.
{{< /callout >}}

We check if `--settings` was provided, and if so, we override whatever was in the environment with the passed argument.
This now allow us to import the `get_server` function, which in turns import the settings module, which is now finally configured.

We can now run our app with
```sh
python -m joe --settings=example_app.settings
```

### Supporting manage.py

We can go a step further, and without having to modify joe we can add a `manage.py` file in our example app to do the same:

```python {filename="example_app/manage.py"}
import os
import sys

BASE_DIR = os.path.abspath(os.path.join((os.path.dirname(__file__)), ".."))
sys.path.append(BASE_DIR)

if __name__ == "__main__":
    from joe.management import execute_from_command_line

    os.environ.setdefault("JOE_SETTINGS_MODULE", "example_app.settings")
    execute_from_command_line()
```

which we can use to run the app with `python example_app/manage.py` directly.

Note that we had to use a bit of path trickery to get this to work - this is because joe is not "installed" as a dependency, so we need to manually add the base dir to the python path for it to be discovered.

## Recap

In this chapter we've created a (non-lazy) settings module, allowed it to be configured in a bunch of ways and we can now start our Joe website in a similar way as we do with Django.

In the next chapter we'll look at how we can provide a more useful framework by  providing useful request information to the views.
