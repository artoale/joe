---
date: '2024-11-11T21:25:16+01:00'
draft: false
linkTitle: 'Getting Started'
title: 'Chapter 0: Getting Started'
breadcrumbs: false
weight: 1
---

Let's make sure you have everything you need to follow along (which isn't much)

## Project setup

You can follow along the snippets or the [companion code](https://gitlab.com/artoale/joe),
or if you, like me, learn best by doing, you can tag along and try to write the
code yourself.

This project also assumes a linux/macos shell, but as long as you know how to
run python programs in your system, all is good 😀

### Prerequisites

This project assume you have basic programming, python and django knowledge.
You don't need to be a Django expert, but you should at least have done something like
the [Django Tutorial](https://docs.djangoproject.com/en/5.1/intro/tutorial01/).

As I've worked on Joe, I've been using python 3.11, so that version should definitely work, but
any fairly recent (3.8+) python version should work fine.

### Project structure

Create a new `learning_joe` folder, this will be your main workspace (you can call
this whatever you like really, I'm going to use `/` to denote the top level directory).
Inside of it, create a `joe` folder with an empty `__init__.py` file in it (this will be the main
module) and an `example_app` folder, which will be a "test" app that uses joe to
build a website.

Your folder structure should look like this

{{< filetree/container >}}
  {{< filetree/folder name="/" >}}
    {{< filetree/folder name="joe">}}
      {{< filetree/file name="\_\_init\_\_.py" >}}
    {{< /filetree/folder >}}
    {{< filetree/folder name="example_app">}}
    {{< /filetree/folder >}}
  {{< /filetree/folder >}}
{{< /filetree/container >}}

