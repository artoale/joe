---
date: '2024-11-11T21:25:16+01:00'
draft: false
linkTitle: 'Database integration'
title: 'Chapter 7: Database Integration'
breadcrumbs: false
weight: 7
---

The whole point of having models is to provide a way to store structured
data in a persistent storage.

Django, like most other python libraries that offer ORM functionality, do this
via the [DB-API 2.0](https://peps.python.org/pep-0249/), a set of APIs intended
to standardise how python code interacts with SQL databases.

This approach involves many steps, including creating an intermediate query object, compiling that to a "flavour" of python that is compatible with a
specific database (in order to support special features) and turning that into
actual SQL, which can then be passed to the relevant implementation of the DB-API.

This goes way beyond the purpose of this project, but I highly encourage you to
look at how Django does this in its source code.

Joe will not be supporting SQL at all, but we'll instead define a simpler
interface that different "database backends" could implement.

## The Base Database

To start with, we define the basic interface a database backend needs to implement in `joe/db/base.py`

```python {filename="joe/db/base.py"}
class BaseDatabase:
    def __init__(self, config):
        self.config = config

    def insert(self, model): ...

    def update(self, model): ...

    def delete(self, model): ...

    def get(self, klass, pk): ...
```

The "config" allows to specify database specific details (e.g. where to store files in a file-based database, the connection string for a SQL database, etc..)

All other methods provide the most basic CRUD functionality -
the ability to perform queries is one notable omission from fairly basic functionality.

### Using the APIs

Following Django's conventions, the database would be configured in settings.py like this:

```python {filename="Python"}
DATABASES = {
    'database_name': {
        'ENGINE': 'path.to.DatabaseBackend',
    }
}
```

And in our code we could then use it as

```python {filename="Python"}
from joe.db import databases
...

db = databases['database_name']
db.insert(...)
db.get(...)
```

### One database per thread

First, we need a way to get the correct database implementation as in the above usage example.

We add this to `joe/db/__init__.py`:

```python {filename="joe/db/__init__.py"}

DATABASE_SETTINGS = "DATABASES"


class DatabaseHandler:
    """
    Registry for thread-local database connections
    """

    def __init__(self):
        self._databases = local()

    def _load_backend(self, backend_str):
        try:
            module_path, module_prop = backend_str.rsplit(".", 1)
            backend_mod = import_module(module_path)
            return getattr(backend_mod, module_prop)
        except (ModuleNotFoundError, AttributeError) as m:
            raise ImproperlyConfigured(
                f"Database backend '{backend_str}' not found"
            ) from m

    def __getitem__(self, alias):
        if hasattr(self._databases, alias):
            return getattr(self._databases, alias)

        try:
            config = getattr(settings, DATABASE_SETTINGS)[alias]
        except KeyError as e:
            raise ImproperlyConfigured(f"Database '{alias}' does not exists") from e

        backend_str = config["ENGINE"]
        backend_cls = self._load_backend(backend_str)
        backend = backend_cls(config, alias)
        setattr(self._databases, alias, backend)
        return backend

    def __setitem__(self, alias, value):
        setattr(self._databases, alias, value)


databases = DatabaseHandler()
```

The only aspect that is worth highlighting now is that we use a `threading.local()`
object to store the database connections instead of a regular dict.

In python, where multiple thread try to access the same object, they typically
get the exact same object. For read-only operations, like reading settings, that's
totally ok! The problem comes when your object could mutate, as different thread
may write conflicting informations.

the `threading.local` object solves that problem because python gives a different
instance of the object to each thread, ensuring no conflict.

This approach is great when you need each thread to have its own copy of the
data, but that wouldn't work when you want threads to actually share data.
We'll see later an example of how to that safely using locks.


## An in-memory db

As our first db we'll look into how we can store objects in memory.
We'll use a dictionary for each table to store our models, indexed by primary
key.

A naive implementation could look a bit like this:

```python {filename="joe/db/memory.py"}

_tables = {}

class InMemoryDatabase(BaseDatabase):
    """Naive in-memory database backend"""

    def insert(self, model):
        table_name = model.__class__.__name__
        table = _tables.setdefault(table_name, {})
        pk = model.pk
        if pk is None:
            pk = len(table) + 1

        if pk in table:
            raise DatabaseError(f"Duplicate primary key {pk}")

        model[pk] = pk
        table[pk] = model

    def update(self, model):
        table_name = model.__class__.__name__
        pk = model.pk
        if not pk:
            raise DatabaseError(f"Cannot update, {model} has no primary key")

        table = _tables.setdefault(table_name, {})

        if pk not in table:
            raise DatabaseError(f"Model with pk {pk} not found")

        table[pk] = model

    def delete(self, model):
        pk = model.pk
        if not pk:
            raise DatabaseError(f"Model {model} has no primary key")

        table_name = model.__class__.__name__
        table = _tables.setdefault(table_name, {})

        if pk not in table:
            raise DatabaseError(f"Model with pk {pk} not found")

        del table[pk]

    def get(self, klass, pk):
        table_name = model.__class__.__name__
        table = _tables.setdefault(table_name, {})
        if pk not in table:
            raise DatabaseError(f"Model with pk {pk} not found")

        return table[pk]
```

This is a good starting point, but it has two big problems:

1. We are storing references to python objects, not a copy of the data, so
whenever a model is changed, it would instantly be changed in the "db" as well
without hitting save
2. This code is not thread-safe - if two requests manipulate the same model,
things could go horribly wrong! For example if two request try to insert an
instance of the same model at the same time relying on the auto-generated PK, they'd
end up getting the same PK assigned and override each other!

Let's see how we can fix these problems, one at a time.

### Storing data, not references

There's many ways we could avoid storing references in our simple db - we
could make a deep copy of them, we could serialise them to some kind of text-based format (like JSON)
or, which is what we'll be doing here, we could use the python built-in and very fast "pickle" module,
which is a way to serialise/deserialise objects to/from a binary representation.

The basic API is fairly simple, we can simply do
`pickled_data = pickle.dumps(model)` when we perform a _write_ operation
and `model = pickle.loads(pickled_data)` when we perform a `_read_`.

so, for example, our `insert` and `get` would become

```python {filename="joe/db/base.py"}
def insert(self, model):
    table_name = model.__class__.__name__
    table = _tables.setdefault(table_name, {})
    pk = model.pk
    if pk is None:
        pk = len(table) + 1

    if pk in table:
        raise DatabaseError(f"Duplicate primary key {pk}")

    model[pk] = pk
    pickled_data = pickle.dumps(model)
    table[pk] = pickled_data

def get(self, klass, pk):
    table_name = model.__class__.__name__
    table = _tables.setdefault(table_name, {})
    if pk not in table:
        raise DatabaseError(f"Model with pk {pk} not found")

    return return pickle.loads(table[pk])
```

We apply this change to all our methods, and we're now ready to move to the next step.

### Making the in-memory db thread-safe

Thread safety is a big, complex topic and many books, courses and learning material exists on the topic, so this is just a very high level overview aimed at fixing the problem at hand.

When two or more threads (units of code execution) try to access the same data, things can go bad, fast.
In general, unless we're experience system programmer, we tend to think of sequence of operations as "atomic" - you read some data, you make a decision based on that data you've read, and maybe you write some data back - making the assumption that nothing will change that data as you're doing so.

This is, in general, not true (when dealing with multi-threading) so we need to protect against it and make our operations behave "atomically".

How can we achieve that?
For our use case, we'll use two python "features"
1. thread-safety of single operations on dictionaries (in CPython)
2. the `threading.Lock` primitive locking objects

Point 1. is somewhat contentious - what counts a single operation in a dictionary? Well, CPython
only allows switching threads between bytecode instructions[^1], so any method call that correspond
to a single bytecode line is safe. We plan on using `dict.setdefault`, which happens to be implemented
in C and thus the bytecode for it simply calls the underlying C function - we're safe!

What about these `threading.Lock` objects?

#### Aside: threading.Lock

Simply put, they are "things" that can be "acquired" only by a thread at a time. Once a thread has
acquired a lock, other threads that try to acquire the same lock will be blocked and will have to wait;
once the initial thread releases the lock, then the next in line will be able to acquire it and proceed.

As with most resources that need to be acquired and then released in python, the standard library
allows us to do that safely thanks to context manager, a type of object that can be acquired through
the `with` keyword, and then gets automatically released by the interpreter when the code block ends or if an exception is raised inside it.

```python {filename="Python"}
with threading.Lock():
    data = read_stuff()
    if data:
        write_stuff()
```

Everything inside the `with` block above can only be "done" by one thread at a time - no thread will
end up interrupting another one after `read_stuff()` has happened, since any thread other than the first
one will be busy waiting on the outside lock.

Now that we know that, we can use it to make our in-memory database much more robust!

#### Using locks in the InMemoryDatabase

The simplest solution I can think of to provide a thread-safe in-memory db would be to
have a single global lock instance and have each operation on the db wait on it, something like this:

```python {filename="joe/db/memory.py"}

db_lock = threading.Lock()

class InMemoryDatabase(BaseDatabase):
    ...
    def insert(self):
        with db_lock:
            ....

    def update(self, model):
        with db_lock:
            ...
    # ...and so on...
```

This would certainly work, but has a slight drawback - we are locking resources much more than we need to!
If a thread is trying to write a `Car` model while another one is reading a `User` model, there's
no reason why the second one should be blocked waiting for the first, as they are accessing different
memory. It's certainly better than a non-thread safe solution, but highly limits the degree of scalability!

At the opposite end of the spectrum you could have locks per individual entities (or per individual "column" on a table) - which is the sort of thing a DBMS would implement.

A quick and easy, but still useful middle ground is to have one lock per table/model class - this way we still allow a reasonable amount of parallelism, without having to implement an incredibly complex system.

Since we don't know which models our app will have before runtime, the easiest way is to create locks
on the fly for each newly accessed table - something like this:

```python {filename="Python"}
locks = {}

...
def _lock_for_table(table_name):
    return locks.setdefault(table_name, threading.Lock())
```

this way, whenever we're operating on a given table, we can do something like

```python {filename="Python"}
with _lock_for_table('table_name'):
    ....
```

and sleep easily knowing that we're ok! (we're ok anyway - remember, this is not a real framework!)


```python {filename="joe/db/memory.py"}
import pickle
import threading
from contextlib import contextmanager

from joe.exceptions import DatabaseError

from .base import BaseDatabase

_tables = {}
_locks = {}


def _lock_for_table(table_name):
    return _locks.setdefault(table_name, threading.Lock())


@contextmanager
def _locked_table(name):
    lock = _lock_for_table(name)
    with lock:
        yield _tables.setdefault(name, {})


class InMemoryDatabase(BaseDatabase):
    """Simple database backend that keeps entities in memory.

    Each "table" is locked independently, and entities are pickled/unpickled to
    support multiple thread accessing the same data.
    """

    def insert(self, model):
        table_name = model.__class__.__name__
        with _locked_table(table_name) as table:
            pk = model.pk
            if pk is None:
                pk = len(table) + 1

            if pk in table:
                raise DatabaseError(f"Duplicate primary key {pk}")

            model[pk] = pk
            pickled_data = pickle.dumps(model)
            table[pk] = pickled_data

    def update(self, model):
        table_name = model.__class__.__name__
        pk = model.pk
        if not pk:
            raise DatabaseError(f"Cannot update, {model} has no primary key")

        pickled_data = pickle.dumps(model)
        with _locked_table(table_name) as table:
            if pk not in table:
                raise DatabaseError(f"Model with pk {pk} not found")

            table[pk] = pickled_data

    def delete(self, model):
        pk = model.pk
        if not pk:
            raise DatabaseError(f"Model {model} has no primary key")

        with _locked_table(model.__class__.__name__) as table:
            if pk not in table:
                raise DatabaseError(f"Model with pk {pk} not found")

            del table[pk]

    def get(self, klass, pk):

        with _locked_table(klass.__name__) as table:
            if pk not in table:
                raise DatabaseError(f"Model with pk {pk} not found")

            pickled_data = table[pk]

        return pickle.loads(pickled_data)
```

## Primary keys

The database code uses `pk` to identify models, but we have no guarantees
that our models have such a field. Let's make sure they do!
As we change the base `Model` class, we can also

```python {filename="joe/db/models.py"}
class Field:
    def __init__(self, primary_key=False, default=None):
        self.default = default
        self.primary_key = primary_key

    # ...

class AutoField(IntegerField):
    """Placeholder field to indicate a db-generated primary key"""
    def __init__(self, **kwargs):
        kwargs["primary_key" = True]
        super().__init__(**kwargs)

    def clean(self, value):
        # No actual validation, but do convert if necessary
        return self.to_python(value)

class Model:
    ...
    _pk_field: Field

    def __init__(self, **kwargs):
        ...
        self._adding = True

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        cls._fields = {}

        # Use a local var to avoid changing __dict__ before iteration is done
        pk_field = None
        for name, field in cls.__dict__.items():
            if isinstance(field, Field):
                cls._fields[name] = field
                if field.primary_key:
                    if pk_field:
                        raise ImproperlyConfigured(
                            "Multiple primary keys are not allowed"
                        )
                    else:
                        pk_field = field

        if pk_field is None:
            pk_field = AutoField()
            cls._fields["id"] = pk_field
            pk_field.__set_name__(cls, "id")

        cls._pk_field = pk_field

    @property
    def pk(self):
        return getattr(self, self._pk_field.name)

    @pk.setter
    def pk(self, value):
        setattr(self, self._pk_field.name, value)

    @classmethod
    def by_pk(cls, pk, using="default"):
        db = databases[using]
        entity = db.get(cls, pk)
        entity._adding = False


    def save(self, using="default"):
        db = databases[using]
        operation = db.update

        if self._adding:
            operation = db.insert

            if not isinstance(self._pk_filed, AutoField) and self.pk is None:
                raise ValueError("Cannot insert model without a primary key")

        return operation(self)
```

A few changes here!

1. We update the Field constructor to allow a field to be marked as primary key
2. We add a _pk_field class attribute to the Model, to identify a (single) field that can
act as primary key
3. We keep track of where the model instance is coming from (is it new? is it coming from a db read?) through the "_adding" instance attribute
3. `__init_subclass__` is updated to look for exactly one pk field. If none is found a placeholder AutoField field is added to the new model
4. We add a pk getter/setter so that you can always do model.pk to access the primary key, regardless of which field name is used for primary key.
5. We implement two method, one for getting a given model by primary key (e.g. `Car.by_pk(12345)`) and one to save a model (e.g. `ford = Car(make="ford"); ford.save()`)

With all these changes in, we're finally ready to try it out!

## Trying it out in our example class

With our newly implemented database, we can see how it works in the example_app.
We'll add two views: one, `car_details` to display the details of a given `Car`,
by id. The other, `cars` will only support POST to create a new car.

Let's start with the latter, so we can create some cars:

First, we add the view code in `example_app/views.py`:

```python {filename="example_app/views.py"}
def cars(req):
    if req.method != "POST":
        raise Exception("method not supported")

    try:
        new_car = Car(
            model=req.json["model"],
            make=req.json["make"],
            plate=req.json["plate"],
            mileage=req.json["mileage"],
        )
        new_car.full_clean()
        new_car.save()
    except Exception as e:
        logger.error(e)
        return HttpResponse(
            '{"error": "database error"}',
            status_code=500,
            content_type="application/json",
        )

    return HttpResponse(
        json.dumps(
            {
                "id": new_car.pk,
                "model": new_car.model,
                "plate": new_car.plate,
                "mileage": new_car.mileage,
                "make": new_car.make,
                "sound": new_car.sound,
            }
        ),
        status_code=200,
        content_type="application/json",
    )
```

and we add the `re_path(r"^api/cars/$", views.cars),` url pattern to `example_app/urls.py`.

We can now start up the server (`python example_app/manage.py`) and try to POST
some data.

```sh
curl -X POST -H 'Content-Type: application/json' -d '{"model":"Bronco", "plate": "XX483MF", "mileage": 99999,  "make": "Ford"}' http://localhost:8000/api/cars/
```

If all goes well, you'll get a response with the newly serialised car. You'll
also see the car has now an `id` assigned to it automatically.

Now we can add one more view to handle retrieval by id. Add this to the
views.py file:

```python
def cars(req):
    if req.method != "POST":
        raise Exception("method not supported")

    try:
        new_car = Car(
            model=req.json["model"],
            make=req.json["make"],
            plate=req.json["plate"],
            mileage=req.json["mileage"],
        )
        new_car.full_clean()
        new_car.save()
    except Exception as e:
        logger.error(e)
        return HttpResponse(
            '{"error": "database error"}',
            status_code=500,
            content_type="application/json",
        )

    return HttpResponse(
        json.dumps(
            {
                "id": new_car.pk,
                "model": new_car.model,
                "plate": new_car.plate,
                "mileage": new_car.mileage,
                "make": new_car.make,
                "sound": new_car.sound,
            }
        ),
        status_code=200,
        content_type="application/json",
    )
```

and the associated url pattern: `re_path(r"^api/cars/(?P<car_id>\d+)$", views.car_details),` (note that we're only matching digits here).

## Recap

We've built a generic interface, for which a custom implementation
could be provided by users of joe. We also provide a thread-safe
in-memory implementation of it!

That's it for now, hope you enjoyed this little adventure.
If you'd like me to expand Joe to cover more aspects of Django,
head over to the [GitLab issue tracker](https://gitlab.com/artoale/joe/-/issues), and let me know there!

[^1]: https://web.archive.org/web/20201108091210/http://effbot.org/pyfaq/what-kinds-of-global-value-mutation-are-thread-safe.htm

