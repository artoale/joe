---
date: '2024-11-11T21:25:16+01:00'
draft: false
linkTitle: 'Handling HTTP Requests'
title: 'Chapter 1: Handling HTTP Requests'
breadcrumbs: false
weight: 1
---

This chapter sets out to create the basic foundations on create a basic HTTP Server in python.

From the start, we significantly deviate from Django itself from an architectural point of view.

Django, like most python web frameworks, doesn't handle HTTP requests directly - it does so via [WSGI](https://peps.python.org/pep-0333/), a python-specific protocol to communicate with web servers (things like gunicorn and uWSGI) - which are responsible for handling http requests, providing
load balancing, running different application in the same process, efficiently
serving static files and more.

Our implementation takes a completely different (and less robust) approach:
use python's own `http.server` module (which is intended for prototyping and not for production use).
This is so we can get started more quickly without needing a lot of boilerplate to start handling HTTP Requests.

{{< callout type="warning" >}}
This alone is a good reason not to use Joe in production. the `http.server` module is [explicitly unsafe](https://github.com/python/cpython/blob/main/Lib/http/server.py#L21).
{{< /callout >}}


## Basic handling of HTTP requests

We start with the simplest possible web server implementation. Python's `http.server.HTTPServer` takes one argument in its constructor, a request handler class,
and provides a utility base class, `http.server.BaseHTTPRequestHandler`, which we can use to handle http requests without worrying about the details of the protocol. BaseHTTPRequestHandler subclasses are expected to implement methods of the form `do_NAME` where name is an HTTP verb - so for handling `GET` requests, we need to implement the `do_GET` method.

Here's a minimal example to get us started:

```python {filename="joe/server.py"}
from http.server import BaseHTTPRequestHandler, HTTPServer


class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        msg = b"Hello, from Joe!"
        # First we send back the status code (200 = success)
        self.send_response(200)
        # Then we tell the client how long is the message, in bytes
        self.send_header("Content-Length", str(len(msg)))
        # We then need to be explicit - there are no more headers
        self.end_headers()
        # Finally we write the message to the output binary file object
        self.wfile.write(msg)


server = HTTPServer(("localhost", 8080), RequestHandler)
server.serve_forever()
```

Save this in a python file `joe/server.py` and run it with `python joe/server.py`.
You can now point your browser to [http://localhost:8080](http://localhost:8080), you'll see the message "Hello, from Joe!" - exciting!

Note that we must specify the content length, in bytes and that the file object we write to (wfile) is a binary file, so we must write binary content to it. We'll soon make this easier.

### Multiple requests
{{< callout type="info" emoji="❓" >}}
If you try to open that page in multiple tabs, on most modern browser, the
second tab will not work. Why?
{{< /callout >}}

Modern browsers typically keep the connection to the web server
open to speed up successive requests. Our server is single-threaded so it's being blocked by the
first connection and cannot handle multiple request. We can fix this easily by replacing our `HTTPServer`
with a `ThreadingHTTPServer` like this:

```python {filename="joe/server.py"}
# ...
server = ThreadingHTTPServer(("localhost", 8080), RequestHandler)
server.serve_forever()
```

If you try again to open [http://localhost:8080](http://localhost:8080) on multiple tabs, now it works!

### A unified handler

When you write a Django view, you typically write one per page, but the same view receives
requests regardless of HTTP method used.
Let's add handlers for some more HTTP verbs, delegating the handling to a single method:

```python {filename="joe/server.py"}
class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self._handle_request("GET")

    def do_POST(self):
        self._handle_request("POST")

    def do_PUT(self):
        self._handle_request("PUT")

    def do_DELETE(self):
        self._handle_request("DELETE")

    def _handle_request(self, verb: str)
        msg = b"Hello, from Joe!"
        # First we send back the status code (200 = success)
        self.send_response(200)
        # Then we tell the client how long is the message, in bytes
        self.send_header("Content-Length", str(len(msg)))
        # We then need to be explicit - there are no more headers
        self.end_headers()
        # Finally we write the message to the output binary file object
        self.wfile.write(msg)
```

You can try this by using the command line tool `curl`

```sh {filename="Shell"}
curl -X POST http://localhost:8080
```

While the almost identical methods work fine and are easy to understand, they are
a repetition, which isn't ideal, and could lead to error 😱! In fact, when I first
wrote the code, I copy-pasted the code, and forgot to change the string parameter
on one of them, so that do_PUT would actually pass the string `"POST"`.
Easily spotted, but a bug nonetheless.

Let's take this as an opportunity to look at some very basic meta programming
in python, the `__getattr__` method.

{{% details title="On dunder methods" closed="true" %}}

Many python meta programming techniques involve implementing methods of the form
`__method-name__`.

These are often referred to as "dunder methods"
(short for double underscore). The naming may seem a bit weird, but the convention is that
your own "private" field should start with a single underscore, while python's language
own field start and end with a double underscore, to avoid naming clash.

{{% /details %}}

When python tries to access an attribute `foo` on an object that
does not define it (and its class and superclass don't either) the interpreter checks
if the object implements the `__getattr__` method. If it does, it calls it passing the name
of the attribute as a string (in this case, `"foo"`).

The `__getattr__` implementation then becomes responsible of deciding if it
wants to return something, or raise an `AttributeError`.

Here's how we could implement `__getattr__` to reduce duplication:

```python {filename="joe/server.py"}
import functools

class RequestHandler(BaseHTTPRequestHandler):
    ...

    SUPPORTED_METHODS = ["GET", "POST", "PUT", "PATCH", "DELETE"]

    def __getattr__(self, name: str):
        if name.startswith("do_"):
            method = name[3:]
            if method in self.SUPPORTED_METHODS:
                return functools.partial(self._handle_request, method)

            raise AttributeError(f"HTTP verb '{method}' not supported")
        else:
            cls = type(self)
            raise AttributeError(f"'{cls.__name__!r}' object has no attribute {name!r}")

```

We only provide a default implementation for attribute of the form `do_<METHOD>` where
`<METHOD>` comes from an allow-list of accepted strings.
If the method called is for one of the supported one, we return a function that,
when called, will call `_handle_request` with the provided string as parameter.

If someone tries to access another undefined attribute (for example call `do_SOMETHING_BAD`)
we raise an attribute error just like the interpreter would.

{{% details title="What is `functool.partial`?" %}}

Partial application is a concept that comes to python from functional programming
languages.

Simply put `functool.partial` is a function that takes as argument:

1. another function `x`
2. one or more arguments that will be passed to that function

and return a **new** function which, when called will call the original `x` function
with the passed in function, along with any other additional argument.

For example:

```python {filename=Python}
import functools

def add(a, b):
    return a + b


add_two = functool.partial(add, 2)

assert(add_two(5) == 7)
```
{{% /details %}}

## Request and Response objects

In Django, the handling of requests is ultimately done by views, callables which accept an
`HttpRequest` object and must return an `HttpResponse` object.

To start off, let's look at how we'd like to use these object in an hypothetical view

```python {filename=Python}
def index(request, response):
    if request.method == "GET"
        return HttpResponse("Hello!")
    else:
        # 405 = method not allowed
        return HttpResponse("Sorry, can't do that", status_code=405)
```

### A trivial HttpRequest

We don't yet need to have a very complex request object, so we can start with
a trivial implementation like this:

```python {filename="joe/request.py"}
class HttpRequest:
    def __init__(self, method: str, path: str):
        super().__init__()
        self.method = method
        self.path = path
```


This version simply stores the method and path, but doesn't yet provide query
parameters, file information or request headers, we'll add those in a [future chapter](./4_http_requests_and_querydict.md).

### HttpResponse

The response object needs to be a bit more complex, as we'd like for it to support
the following features:

* Raw bytes content
* String content (with arbitrary encoding)
* Automatic generation of "Content-Type" and "Content-Length" headers where possible

The code for it is a bit long, but hopefully fairly straightforward:

```python { filename="joe/response.py" }
class HttpResponse:
    """Basic HTTP Response

    Handles some limited common cases, like sending back html by default (like Django's)
    """

    def __init__(
        self,
        content = b"",
        status_code = 200,
        content_type = "text/html",
        charset = "utf-8",
        headers = None,
    ):
        super().__init__()

        # just an attribute
        self.charset = charset

        # Setting headers first...since other property setters use them
        if headers is None:
            self.headers = {}
        else:
            self.headers = headers.copy()

        # Setting defaults for internals
        self._status_code = 200
        self._content_type = "utf-8"

        # These are properties, setting them last, setters may raise, let them
        self.content = content
        self.status_code = status_code
        self.content_type = content_type

    @property
    def status_code(self) -> int:
        return self._status_code

    @status_code.setter
    def status_code(self, code):
        self._status_code = int(code)

    @property
    def content(self) -> bytes:
        return self._content

    @content.setter
    def content(self, val: Union[bytes, str]):
        """Setter for content

        Encodes the passed string if necessary, and sets the content-length header
        accordingly. The right way of doing this would be lazily on the getter,
        but it would make setting the headers automatically more complex.
        """
        if isinstance(val, bytes):
            self._content = val
        else:
            self._content = val.encode(self.charset)

        self.headers["Content-Length"] = str(len(self._content))

    @property
    def content_type(self) -> str:
        return self._content_type

    @content_type.setter
    def content_type(self, val: str):
        """Setter for content-type

        Also sets the Content-Type header accordingly in the format:
        Content-Type: <content_type>; charset=<charset>

        This doesn't really work unless you set the charset first, and as per
        content setter it should be done lazily.
        """
        self._content_type = val
        self.headers["Content-Type"] = f"{self._content_type}; charset={self.charset}"
```

While this class is still relatively basic, you may have noticed there's a lot of `@property` decorator, which we hadn't encountered before. `@property` decorators are arguably the easiest meta-programming tools that python provides,
and we're using it here to automate the setting of some headers.

In a nutshell, a python `@property` behaves as an attribute when seen from the outside world, but the implementation can handle both getting and settings the values in a dynamic way: it can perform validation, transform the value to match a preferred internal representation or, as is the case if the `.setter` method is not implemented, prevent assignment.

Other languages have developed "good practices" recommending all attributes to be private and using
methods to manipulate them (think getters and setters in Java). In such languages, this is good advice,
as making a private property private "later" can be a big refactoring task.

The beauty of python and the `@property` decorator is that it renders that completely unnecessary!
If we'd like for an attribute to be writeable without further checks, we just add it to the object,
and if we later we decide we need to perform some side effects or transformation when the value is,
set we can easily change the internal representation without breaking code that uses that object, neat!

{{< callout type="info" emoji="🖌️" >}}
Another advantage of using properties is that the surface API is a lot cleaner for some kind of operations.
For example, doing `object.value += 1` is much more readable than `object.set_value(object.get_value() + 1)`
{{< /callout >}}

In our example, we store the actual values in _"private"_ (by convention) attributes
which we return when the property getter is called (e.g. `status_code = response.status_code`).

When the attribute is assigned to, it's were the magic happens, since we
ensure the correct headers are set, that the content is encoded to binary using the right charset and so on.

An alternative approach could have been to "lazily" compute the derived value through getters - this
is probably a better approach (as we would be computing things only when needed), but it makes the
code a little bit easier to follow, so we'll keep it as is for the time being.

## Dispatching requests to views

Now that we have an abstraction for requests and response, we can put them together in our server.

Instead of doing all the work in the `RequestHandler`, we delegate the job of dispatching a
request to the right view to a `Dispatcher` object (just like Django does).

For now, the Dispatcher will have a single method `dispatch(request: HttpRequest) -> HttpResponse`
We can change our handler code to use it (we'll implement it right after):

```python { filename="joe/server.py" hl_lines=[3,14, 30, 32], linenostart=1}
class RequestHandler(BaseHTTPRequestHandler):
    def __init__(self, dispatcher: Dispatcher, *args, **kwargs):
        self._dispatcher = dispatcher
        super().__init__(*args, **kwargs)

    ...

    def _handle_request(self, verb: str)
        request = HttpRequest(verb, self.path)

        # We want error handling to be done by the dispatcher
        # which will not raise here. If it does, let it blow, it's a bug in
        # the framework, not in client code.
        response = self._dispatcher.dispatch(request)

        # Status code is set on the response object
        self.send_response(response.status_code)

        # And so are all the headers
        for header, value in response.headers.items():
            self.send_header(header, value)

        # TODO: Django also adds any cookies based on the response object here, we don't for now
        self.end_headers()

        # Finally we send the encoded content back
        self.wfile.write(response.content)


dispatcher = Dispatcher() # we'll want to do some configuration here.
# We use partial here to bind the dispatcher to the request handler constructor
server = ThreadingHTTPServer(("localhost", 8080), functools.partial(RequestHandler, dispatcher))
server.serve_forever()
```

To finish off the final chapter, let's see how we could implement a basic version of it:

```python { filename="joe/dispatcher.py"}
# We provide a couple of "demo" views for now, one which succeeds...
def _dummy_view(req: HttpRequest):
    logger.info(f"View called with {req!r}")
    return HttpResponse(f"Thanks for {req.method}ing from joe!")

# ...and one which intentionally fails
def _dummy_view_with_exception(req):
    raise Exception("I am a failing view!")

class Dispatcher:
    def __init__(self) -> None:
        # This will need to be extracted in some sort of URL->view mapping
        # system (the main objective of the next chapter).
        #
        # For now, we hardcode a mapping between paths and view functions.
        self._routes = {
            "/": _dummy_view,
            "/error": _dummy_view_with_exception,
        }

    def dispatch(self, request: HttpRequest):
        path = request.path
        # If we haven't configured the url, return a 404
        if path not in self._routes:
            return HttpResponse("Page not found", status_code=404)

        try:
            return self._routes[path](request)
        except Exception as e:
            # If the view raises an exception, return a 500
            logger.error("Internal Server Error: %s", e, exc_info=True)
            return HttpResponse("Internal server error", status_code=500)
```

As you can see, `dispatch` can do a lot of useful things, from returning a
404 response for request paths that we don't have in our site, to handling exceptions
in a buggy view by catching errors and returning a consistent error message and status code.

## Final wire up

Before we wrap this chapter up, let's do a bit of clean up so we can use `joe` as a proper python module.

First we change the `server.py` file to export the server instead of running it directly

```python {filename="joe/server.py"}
...

def get_server(host: str, port: int) -> HTTPServer:
    address = (host, port)
    dispatcher = Dispatcher()

    handler = functools.partial(RequestHandler, dispatcher)

    return ThreadingHTTPServer(address, handler)
```

we then add a `joe/__module__.py` file

```python {filename="joe/__module__.py"}
import logging
import sys

from joe.server import get_server

logger = logging.getLogger("joe")

if __name__ == "__main__":
    host = "localhost"
    port = 8080

    logger.info("Starting http server")
    server = get_server(host, port)
    try:
        logger.info("Listening on %s:%d", host, port)
        server.serve_forever()
    except KeyboardInterrupt:
        logger.info("Received Keyboard interrupt, shutting down")
    except OSError as e:
        logger.error("Error while creating HTTP server %s", e)
        exit(2)
```

Now we're finally done!
We can run `joe` as a python module:

```sh {filename="Shell"}
python -m joe
```

which is cleaner, and allows us to keep CLI configuration and handling separate
from the server.

## Recap

That was a lot!
To sum it up we have:

1. Written a simple server using `ThreadingHTTPServer` and `BaseHTTPRequestHandler`
2. Implemented a nice abstractions over the concept of http request and response
3. Laid the foundations for routing to user-provided views

Along the way, we've explored a few python concepts, in particular:

1. How `__getattr__` can provide default implementation when an undefined attribute is accessed
2. Using `functool.partial` to create new functions on the fly
3. How to use the `@property` decorator to control access to object attributes

If you followed along, your project should look a bit like this:

{{< filetree/container >}}
  {{< filetree/folder name="/" >}}
    {{< filetree/folder name="joe">}}
      {{< filetree/file name="\_\_init\_\_.py" >}}
      {{< filetree/file name="server.py" >}}
      {{< filetree/file name="dispatcher.py" >}}
      {{< filetree/file name="request.py" >}}
      {{< filetree/file name="response.py" >}}
    {{< /filetree/folder >}}
    {{< filetree/folder name="example_app">}}
    {{< /filetree/folder >}}
  {{< /filetree/folder >}}
{{< /filetree/container >}}

The [part-1-http-reques branch](https://gitlab.com/artoale/joe/-/tree/part-1-http-request) contains
some extra tweaks, as well as unit tests, if you're interested.

Next up, we'll be looking at implementing views!
