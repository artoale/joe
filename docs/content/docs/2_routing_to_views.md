---
date: '2024-11-11T21:25:16+01:00'
draft: false
linkTitle: 'Routing to views'
title: 'Chapter 2: Routing to views'
breadcrumbs: false
weight: 2
---

In this chapter we'll take a look at how we can provide a configurable mapping
between urls and views, including passing positional and named parameters.

The goal is to provide somwething like django `URLconf` mechanism, which comes
in two flavours: `path` and `re_path`.
Under the hood, `path` matching is transformed into regular expression matching,
so we'll pick `re_path` as the candidate for our implementation.

{{< callout type="info" emoji="💭" >}}
On the topic of mapping URLs to view, it's interesting to note that another approach
is to use decorators to "register" a view. This keeps the view and URL more coupled,
but also makes the relationship easier to spot (many other frameworks, like flask and FastAPI do this).

If you're looking for an exercise, building a decorator-based system to configure Joe routes
(or, why not, Django's!) would be quite fun!
{{< /callout >}}

## A look at Django's `re_path`

Let's have a look at what we're trying to build.
The `django.urls.re_path` has this signature:

```python { filename="Python" }
def re_path(route, view, kwargs=None, name=None)
```
where `route` is the path (typically a raw string representing a regular expression)
`view` is either the view we want to map to this route, or a "nested" url
configuration to be hooked at this base url, `kwargs` are any extra parameter we
want to pass to the view (for example if we're using the same view function in
different context) and `name` is a string that we can pass to the `reverse()`
function to get the full URL without having to write the whole thing down.

_Note: `reverse` basically requires reverse-engineering the regex. We won't be doing that._

_Further Note: Django also allows for the `route` to be a lazily translated string. We won't._

Here are some example usage that we will supported in Joe:

```python { filename="Python" }
debug_url_patterns = [...]

urlpatterns = [
    re_path(r'^$', views.home),
    re_path(r'^articles/(?P<article_id>\w+)/$', views.article),
    re_path(r'^drafts/(?P<article_id>\w+)/$', views.article, kwargs={"draft": True})
    re_path(r'^admin/', include('admin.urls')),
    re_path(r'^debug/', include(debug_url_patterns)),

    # Django supports this, but it's discouraged in the doc, so we won't support it
    # re_path(r'^archive/(\d\d\d\d)/(\d\d)/$', views.archive, name="archive"),
]
```

From top to bottom:
- A route without any capturing group
- A route with a named capturing groups, which will be passed as named arguments
- As before, but additionally passing extra kwargs to the view
- A route that serves as the "hook" point for another url configuration which we include here
- Same as before, but with the list passed in directly rather than loaded from a module

Also (commented out) a route with capturing groups, which we will not support as
it's discouraged.

Django does a lot more, including providing a simpler interface via `path`, supporting multiple apps,
namespaces, locales, and a simple `name` argument that can be used for easily retrieve a view/url from a mnemonic name.
To keep things simple, we won't be supporting any of these features.


## Data Structure

Let's first think about what what would be a good data structure to hold
the data, so that it's use to go from an actual URL to a given view, and we'll
later think about how we can build that data structure.

The URL config we defined above can be seen as a tree - the root urlconf would be the tree root, each module exporting a urlpatterns would be a subtree, and views would be tree leaves.

We'll then have two classes, one to represent a leaf, a simple mapping between a pattern and a view,
and one that represent the root of a subtree, called respectively (following Django's naming) `URLPattern` and `URLResolver`.

Both need to be able to support the same operation: given a `path` as a string it should return the matching view (and keyword arguments) if itself of any of its subtrees are a match.

### Leaf nodes: `URLPattern`

We can now create a new module, `urls.py` and start by implementing the code for a leaf node to begin with:

```python {filename="joe/urls.py"}
import re

class URLPattern:
    def __init__(
        self,
        pattern,
        view,
        extra_kwargs=None,
    ):
        self.pattern = pattern
        self.view = view
        self.regex = re.compile(pattern)
        self.extra_kwargs = {} if extra_kwargs is None else extra_kwargs

    def resolve(self, path):
        match = self.regex.search(path)

        if match:
            # As per django, extra_kwargs take precedence so listing them afterwards
            kwargs = {**match.groupdict(), **self.extra_kwargs}
            return (self.view, kwargs)

```

The code for this node is relatively easy: we compile the pattern at init time, and when asked to resolve a URL we search the provided path for a match.

{{< callout type="info">}}
Here we're relying on the provided regex to use delimiters like `^` and `$` appropriately, as Django does, so using `search` provides the correct behaviour. We could have used `fullmatch` and ensure any "leaf" node specifies the full regex, but we'll stick to Django rules here.
{{< /callout >}}

If we find a match, we return a tuple with the stored view as first item, and a dictionary with any kwargs to be passed to it as second. This dictionary will include any extra kwargs passed to `re_path`.

### Tree nodes: `URLResolver`

The `URLResolver` represent a generic non-leaf node. As such,
it won't take a view object at init time, but an iterable of
tree nodes. The resolve logic is also a bit more complex, since here we're not only doing the matching for the "current" node, but also for all the relevant sub-nodes.

This is what it could look like:

```python {filename="joe/urls.py"}
class URLResolver:
    def __init__(
        self,
        pattern,
        urlpatterns,
        extra_kwargs = None,
    ):
        self.pattern = pattern
        self.urlpatterns = urlpatterns
        self.regex = re.compile(pattern)
        self.extra_kwargs = {} if extra_kwargs is None else extra_kwargs

    def resolve(self, path):
        # Does the provided path match (partially) with the
        # resolver pattern itself?
        match = self.regex.search(path)

        if match is None:
            # No match, this is not the right subtree
            return

        kwargs = match.groupdict()

        # Get the remaining part of the regex
        end = match.end()
        sub_path = path[end:]

        # For each sub-node...
        for pattern in self.urlpatterns:
            # ...check if it's a match
            sub_match = pattern.resolve(sub_path)

            if sub_match:
                # If yes, merge the return kwargs with the ones from this resolver
                view, sub_kwargs = sub_match
                all_kwargs = {**kwargs, **self.extra_kwargs, **sub_kwargs}
                return (view, all_kwargs)
```

The code for the resolver is only a bit more complex than the leaf case.

We first check if the node itself has a partial match. If not, we stop, since there's no point in checking the subtree.
If there is a partial match, we use the remaining (non-matched) part of the path and we delegate matching to each child node recursively.

If a full match is found, we merge all the arguments and return the view-kwargs tuple.

## The public APIs

Now that we have the underlying data structures set up, we can build up the two functions that our users will call to build up URLs hierarchies: `re_path` and `include`. We'll start from the latter, since it returns arguments used by the first.

### Including sub-modules: the `include()` function.

Given our very limited set of operations supported, the `include` function ends up being quite easy. It supports two modes: `include('path.to.url')` to include URLs by module dotted path and `include(urlpatterns)` where `urlpatterns` is a list or tuple of nodes in our hierarchy.

The code follows:

```python {filename="joe/urls.py"}
from importlib import import_module

def include(urlpatterns):
    if isinstance(urlpatterns, str):
        module = import_module(urlpatterns)
        return module.urlpatterns

    if isinstance(urlpatterns, (tuple, list)):
        return urlpatterns

    raise TypeError(
        f"Argument must be a string, list or tuple. {type(urlpatterns)} provided."
    )
```

First, we check if the argument is a string. If yes, we load the module using the standard library "importlib" module and return its `urlpatterns` attribute.

Otherwise, we check that the argument is either a list or tuple and return it as is.

Error handling could be improved here (for example by raising something like django's `ImproperlyConfigured` exception if the module loadin fails, or if the module doesn't expose a "urlpattern" attribute), but the default exception here should provide enough of a hint to our users.

The use of `importlib.import_module` is worth a quick stop as it's quite powerful and widely used by Django. When you write some python code, you
typically use the `import module_name` statement to bring in other modules
that you want to use in your code. This requires you to know the module name
when you write code, but what if that isn't known up until runtime?

Enter `importlib.import_module`, which provides a simple way to perform the
same `import` operation, but at runtime.

### The main client API: `re_path`

All the routing logic is handled nicely by the recursive data structure, so the only job left for `re_path` to do is to build the right node depending on what we pass it.

```python {filename="joe/urls.py"}
# ...
def re_path(route, view_or_include, kwargs=None):
    if callable(view_or_include):
        return URLPattern(route, view_or_include, extra_kwargs=kwargs)

    else:
        return URLResolver(route, view_or_include, extra_kwargs=kwargs)
```

If the second argument is a function (or something that can be called, like an object implementing `__call__`) we build a leaf node, otherwise it means that we're getting a list or tuple of nodes and we group them in a subtree by building a `URLResolver`. That's it!

You can have a peak at [`test_urls.py`](https://gitlab.com/artoale/joe/-/blob/main/tests/test_urls.py) to see that these are working as intended.

We're missing one important step though: piecing it all together.


## Putting it all together

It's finally time to see our toy framework start to behave like one.


### The example app

If you haven't already, create an `example_app` directory next to `joe`, then
add an `__init__.py`, a `urls.py` and a `views.py`.

We'll start with three simple views:

```python {filename="example_app/views.py"}
import logging

from joe.response import HttpResponse

logger = logging.getLogger(__name__)


def home(req):
    logger.info("Home called")
    return HttpResponse("Welcome to the example app!")


def details(req, details):
    logger.info("Details called")
    return HttpResponse(f"Details: {details}")


def error(req):
    logger.info("Errors called")
    raise Exception("I am erroring")
```

We have three views:
* `home`, which returns a hardcoded string
* `details`, which returns whatever is passed as argument
* error, which intentionally raises an exception (so we can check out error handling).

The `example_app.urls` module provides the urlpatterns, and should look familiar as it looks very close to a regular Django urls config:

```python {filename="example_app/urls.py"}
from example_app import views
from joe.urls import re_path

urlpatterns = [
    re_path(r"^$", views.home),
    re_path(r"^details/(?P<details>[\w-]+)/$", views.details),
    re_path(r"^error/$", views.error),
]
```

### Updating the `Dispatcher`

In the previous chapter we hardcoded routes and views in the dispatcher.
We can now improve that by making it much more dynamic:


```python {filename="joe/dispatcher.py"}
from joe.response import HttpResponse
from joe.urls import URLResolver, include

class Dispatcher:
    def __init__(self):
        self.root_resolver = URLResolver(r"^/", include("example_app.urls"))

    def dispatch(self, request):
        path = request.path

        resolved = self.root_resolver.resolve(path)

        if not resolved:
            return HttpResponse("Page not found", status_code=404)

        view, kwargs = resolved

        try:
            return view(request, **kwargs)
        except Exception as e:
            logger.error("Internal Server Error: %s", e, exc_info=True)
            return HttpResponse("Internal server error", status_code=500)
```

At instantiation, we create a URLResolver that catches all requests (the path always starts with `/`) and uses the test url modules as the entry point.
The dispatch method uses the resolver to find a view - returning a 404 if no view
matches. If there is a matching view, we call it with any provided arguments, catching
and reporting any exception as a 500 error.

If we now run the project (you can always checkout the repo branch `part-2-routing-to-views` and run `python -m joe`) we can test it with a few curl commands:

```shell {filename="Shell"}
$ curl localhost:8080/
Welcome to the example app!

$ curl localhost:8080/error/
Internal Server Error
# Note the server logs, you should see the correct exception being raised

$ curl localhost:8080/details/i-am-joe/
Details: i-am-joe
```

Yay! While there's multiple enhancement we could make, we'll keep it simple
for now.

## Recap

In this chapter we have

* Designed a recursive data-structure to represent an arbitrary site map
* Implemented a subset of Django's url configuration APIs
* Created an example app, which will allow us to better test new features as we add them

Along the way we've seen how to import modules dynamically, and how you can think
about recursive data structures in a very simple way.

You may have noticed that currently the example app url config path is still
hardcoded in `joe/dispatcher.py` which is no good, we need a way to make this dynamic.

In the next chapter we'll see how we can reproduce django's settings system and
truly separate the application code from the framework - read on!
