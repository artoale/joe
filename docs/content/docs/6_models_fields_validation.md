---
date: '2024-11-11T21:25:16+01:00'
draft: false
title: 'Models, Fields, validation'
title: 'Chapter 6: Models, Fields, validation'
linkTitle: 'Models, Fields, validation'
breadcrumbs: false
weight: 6
---

We're now ready to start looking at one of Django's most powerful features: its ORM (Object-Relational Mapper).

ORMs are a layer that allow to access a relational database through Object
Oriented programming (OOP) paradigms.

Django's approach is to have a 1-1 mapping between objects representing business
logic data to rows in the database, an architectural design pattern known as "Active Record".

Understanding all the intricacies of Django's models goes way beyond the scope
of this project. In Joe, we're going to implement a very simplified version
that can be understood more easily, while still retaining its core functionality.

This is a high level view of what we'd like Joe models to support:

* Definition: define classes that can be mapped to tables, with fields as columns
* Validation: provide a way to specify rules that field need to comply with and validate against them
* Saving: store the created model on some kind of database
* Retrieval: fetch a model from the database

{{< callout type="info" >}}
This section looks into some fairly advanced python meta-programming techniques.
Most of these techniques are not commonly used when you write "regular" python user, and are typically
used by library and framework developers.
{{< /callout >}}

## Model definition

Let's start by having a look at how model definition could work in Joe, and we'll assume a
generic `Field` for now

```python {filename="Python"}

class Car(Model):
    model = Field("model")
    make = Field("make")
    plate = Field("plate")
    mileage = Field("mileage")
    sound = Field("sound", default="Honk!")

print(Car._fields) # prints something like { 'model': CharField(..) , 'make': CharField(..) , ...}

focus = Car(model="Focus", mileage=100_000)
focus.plate = "AB483MF"

print(focus.model)    # prints "Focus"
print(focus.plate)    # prints "AB483MF"
print(focus.mileage)  # prints 100000
print(focus.sound)  # prints "Honk!"

print(focus.__dict__) # prints something like "{ 'model': 'Focus', plate: 'AB483', ...}"

```

Hopefully this API seems fairly natural, and if you have worked with python for
a while it shouldn't be overly surprising.

There is one aspect that may be somewhat surprising though:
we're defining attributes (model, plate, etc..) at the class level, but those
get somewhat magically turned into individual values on the instance.

How's that possible? Moreover, the Car._fields attribute holds a dictionary with all
the fields declared on that model class, where does that come from?

It's not entirely impossible to support this behaviour with the tools
we've seen before (e.g. `__getattr__`), but it'd be quite cumbersome and, as we'll soon
see, we'll want to do more than the above.

Django does this by using metaclasses which are probably the most powerful,
but also one of the most confusing meta programming techniques available to
python engineers.

In Joe, we'll use a simpler feature, which has been introduced to the python
language since version 3.6: the `__init_subclass__` hook.

{{< callout type="info" >}}
`__init_subclass__` is a fairly new API, and while not as powerful as meta-classes
it still cover a very large proportion of the use cases you'd previously use meta-classes for
with a much cleaner API.
{{< /callout >}}

### The `__init_subclass__` dunder method

When a class `Foo` which inherits from a class `Base` is **defined**, the
python interpreter calls the `Base.__init_subclass__` static method (if it is defined).

Note that this is called when the class is defined, not when an instance of
it is instantiated.
This allows to manipulate the class itself, add or change
class attributes, validate those, and much more.

Looking at our example code above, we can use that at our advantage to save information about our model fields on
the class itself, as a dictionary stored on the `_field` attribute, for later use.

{{% details title="On classes and types" closed="true" %}}

In python, everything is an object: values, like int, str and dict are all
instances of a class, but not only that - also functions, methods and classes
themselves are objects! You can check what classes something is an instance of by calling `type(thing)`.

This means that even classes are instance of classes! The `type` class is
by default the type of a regular class (unless it specifies a meta-class). The class of a class
is responsible for instantiating and initialising classes. You can think of `__init_subclass__`
as being called by `type` as part of its `__init__` method, when it is instantiating any
subclass of `Base`.

{{% /details %}}

### Implementing `__init_subclass__`

First, let's create a new submodule `joe.db` and add a new `models.py` file
were we'll add the two classes we used in our example code above

```python {filename="joe/db/models.py"}
class Field:
    """Bare bone field class, just stores the default for now"""
    def __init__(self, default=None):
        self.default = default

class Model:
    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)

        # add a _fields dictionary on the subclass to store the fields "config"
        cls._fields = {
            name: value
            for name, value in cls.__dict__.items()
            if isinstance(value, Field)
        }

    def __init__(self, **kwargs):
        # Initialise all the fields...
        for name, field in self._fields.items()
            # using the field default if value not specified
            value = kwargs.pop(name, field.default)
            setattr(self, name, arg)

        # If the model is constructed with values that are not fields raise
        if kwargs:
            raise TypeError(f"Invalid argument(s): {','.join(kwargs)}")
```

The `Field` class just stores the default value passed at init time, so not
much happens there.

The `Model` class is where the fun begins. The `__init_subclass__` receives
the subclass being defined as first param (by convention, we call that `cls` to make
it clear it's not a regular method).

In it, we inspect `cls.__dict__` which contains all the (sub) class fields and
use that to add any that is an instance of `Field` to the `_fields` dict.

Then, in the regular `__init__` we can use that to populate instance attributes.
We iterate over the `_fields` and check if a value was passed to the constructor.
If it was, we use that, otherwise we use the default stored on the field.

If kwargs has any extra kwargs, we can then raise an exceptions, since it's not
going to be a known field.

The code above is short, but it can be a bit confusing. I recommend trying it out,
maybe changing to see hwo things behave differently, as well as adding the "`Car`"
example from above in the same file and run it as a python script, to see it in action.

## Model validation

With the basic of model definition covered, we can then move to how validation could work.

Again, let's look at the API we'd like to support first:

```python {filename="Python"}
class Car(Model):
    model = CharField(max_length=10)
    make = CharField(max_length=10)
    plate = CharField(max_length=7)
    mileage = IntegerField()
    sound = CharField(default="Honk!")

    def clean(self):
        if self.make == "Fiat" and self.model not in (
            "Punto",
            "500",
        ):
            raise ValueError(f"'{self.model}' is not made by '{self.make}'")
        if self.make == "Ford" and self.model not in (
            "Focus",
            "Bronco",
        ):
            raise ValueError(f"'{self.model}' is not made by '{self.make}'")


punto = Car(
    model="Punto",
    plate="ABCD",
    mileage=100_000,
)

punto.clean_fields()  # Raises ValidationError("Field 'Car.make' is required")

punto.make = "Fiat"
punto.clean_fields()  # Does not raise

punto.make = "A long make name that exceeds max_length"
punto.clean_fields()  # Raises ValidationError("Field 'Car.make' is too long (max_length=10)")

punto.make = "Ford"
punto.clean_fields()  # Does not raise, model.clean is not called by clean_fields()
punto.clean()  # Raises ValidationError("'Punto' is not made by 'Ford'")
punto.full_clean()  # Raises ValidationError("'Punto' is not made by 'Ford'")

punto.make = "Fiat"
punto.full_clean()  # Does not raise

punto.mileage = "not_a_number"
punto.clean_fields()  # Raises ValidationError("Field 'Car.mileage' is not an integer (got 'not_a_number')")

punto.mileage = "1234"
punto.clean_fields()  # Does not raise, '1234' is converted to 1234 with int()
assert punto.mileage == 1234  # True
```
Just like Django models, we want to support two levels of validation:
* Field level: `clean_fields` checks that each field is valid on its own
* Model level: `clean` which is a hook your models can implement to perform model-level validation

We also want to provide `full_clean`, which performs both.

Also worth nothing that, like django, fields can transform the data they get from a form, an API call, or the database - to their "correct" python type.
Notice how, when we assign the string `"1234"` to the mileage field, it gets
automatically converted to the `1234` number.

Most of the implementation for supporting this is fairly straightforward, let's
look at how it would look like.

First, we add the methods described above to the `Model` class

```python {filename="joe/db/models.py"}
class Model:
    ...

    def clean_fields(self):
        for name, field in self._fields.items():
            value = getattr(self, name)
            setattr(self, name, field.clean(value))

    def clean(self):
        """Subclasses can override to perform custom model validation"""
        pass

    def full_clean(self):
        self.clean_fields()
        self.clean()
```
The `clean` function does nothing, is simply an hook for user-defined model
to provide extra validation, while `full_clean` calls `clean_fields` and `clean` as
described above.
`clean_fields` is the only interesting method, which iterates over the model
fields, and delegates validation to them. If successful, the returned value is
assigned to the relevant attribute of the model instance.

Let's see how the `Field` could handle that validation:

```python {filename="joe/db/models.py"}
class Field:
    def __init__(self, default=None):
        self.default = default

    def clean(self, value):
        """Convert to the correct python object and validates it."""
        value = self.to_python(value)
        self._validate_required(value)
        self.validate(value)
        return value

    def to_python(self, value):
        """Convert to the correct python object."""
        return value

    def _validate_required(self, value):
        if value is None:
            raise ValueError(f"This field is required")

    def validate(self, value):
        """Hook for fields to perform field-specific validation"""
        pass
```
Starting from the bottom:

* `validate` does noting, it just provides a hook for subclasses to override if the need field-specific validation
* `_validate_required` is a validation step that all fields need: if the value is None and they don't have a default, a Value error is raised. Since defaults (if present) are set on the model at init time, they're ignored here.
Django has many more way to configure fields (in particular `blank` and `null`). Implementing them is not hard, but will add boilerplate we don't need to .
* `to_python` is another noop hook that performs conversion to the correct python object representation when required
* `clean`: calls this methods in chain


### CharField and IntegerField

This now allows us to implement CharField and IntegerField:

```python {filename="joe/db/models.py"}
class CharField(Field):
    def __init__(self, max_length=None, **kwargs):
        super().__init__(**kwargs)
        self.max_length = max_length

    def to_python(self, value):
        if value is None or isinstance(value, str):
            return value
        return str(value)

    def validate(self, value):
        if self.max_length is not None and len(value) > self.max_length:
            raise ValueError(
                f"Field is too long (max_length={self.max_length})"
            )


class IntegerField(Field):
    def to_python(self, value):
        if value is None or isinstance(value, int):
            return value
        try:
            return int(value)
        except (ValueError, TypeError) as e:
            raise ValueError(
                f"Field is not an integer (got '{value}')"
            ) from e
```

As you can imagine, `CharField` implements the hooks defined by `Field`. In
particular `to_python` converts the value, if not already a string (or None)
to its string representation (which is a "safe" operation in python, since all
object should have a valid `__str__` implementation, either their own or one inherited from object)
The other hook we override is `validate`, which check the string against
the max_length provided at init, if any.

We're almost there!

### Better errors and `__set_name__`

You could run the example code above now, and it'd _almost_ work, if not for the
fact that the error messages are a bit generic and don't tell us which field
caused the validation error, which is not very useful!

We could fix this by adding a `set_attribute_name(name)` method on the `Field`
class to store the attribute this field instance refers to, and call that
for each field in `Model.__init_subclass__`. While this would bes a viable
option (and Django does something like that in its model metaclass `ModelBase`),
we can instead take full advantage of modern python, and achieve the same
result implementing another new method introduced in python 3.6: `__set_name__`.

This special method is called whenever an object is instantiated as a class
attribute (typically as a descriptor, which we'll see later).

It receives two positional arguments, conventionally named "owner" and "name":
`owner` is the class of which the Field is an attribute of (our `Car` model),
while `name` is the name of the attribute (`model`, `make`, `plate`, etc..).

With that knowledge, we can now implement `__set_name__` and provide better
error reporting in our fields:

```python {filename="joe/db/models.py"}
class Field:
    ...
    def __set_name__(self, owner, name):
        self.name = name
        self.model = owner

    @property
    def full_name(self):
        return f"{self.model.__name__}.{self.name}"

    def _validate_required(self, value):
        if value is None:
            raise ValueError(f"Field '{self.full_name}' is required")

class CharField(Field):
   ...

    def validate(self, value):
        if self.max_length is not None and len(value) > self.max_length:
            raise ValueError(
                f"Field '{self.full_name}' is too long (max_length={self.max_length})"
            )


class IntegerField(Field):
    def to_python(self, value):
        if value is None or isinstance(value, int):
            return value
        try:
            return int(value)
        except (ValueError, TypeError) as e:
            raise ValueError(
                f"Field '{self.full_name}' is not an integer (got '{value}')"
            ) from e
```

Each field instance now has a reference to the model it is attached to. We
added a `full_name` property to make it easier to reference the Model.field
string and use it in our field error message.

We should also raise a joe-specific exception instead of ValueError, let's
do so in `exceptions.py`, and replace it throughout our model class.

```python {filename="joe/exceptions.py"}
class ValidationError(Exception):
    """Error during validation"""
```


## Recap

That's a dense chapter!
We've build the foundation for our model system, adding support for
type conversion, field and model validation. We've also used `__init_subclass__` and
`__set_name__` a powerful, modern python alternative to meta-classes.
