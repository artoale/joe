---
date: '2024-11-11T21:25:16+01:00'
draft: false
# title: 'Docs'
---

# Joe: Build yourself a Django


Hello and welcome!

This project is a learning adventure aimed at studying and exploring how a python web framework
like [Django](https://www.djangoproject.com/) works, learning the basics of some python advanced concepts along the way.

{{< callout type="warning" >}}
**DO NOT ATTEMPT TO USE IN PRODUCTION.**

While `joe` maye seem functional it is by no means a production-ready library.
It is not audited for security and will perform poorly.
For this reason it's not published on pypi.
{{< /callout >}}


## Content Organization

This site is organized in chapters, each focusing on a particular problem and how we could solve it.
Each chapter explore some non-trivial python aspect, like `__dunder__` methods, meta-programming and concurrency.

Each chapter builds on the previous one, so you'll probably find it easier to follow along chapter
by chapter.


### Code snippets

Each chapter contains significant amount of code, but it is sometimes simplified compared to the [full code](https://gitlab.com/artoale/joe)
to favour understandability.

Ready?

<div class="hx-mt-6 hx-mb-6 hx-text-center">
{{< cards >}}
  {{< card link="docs/0_getting_started" title="Getting Started" icon="document-text" subtitle="Prerequisites and environment setup." >}}
{{< /cards >}}
</div>


## Contribution

Found a bug or typo? Want to see more? Head over to GitLab and file a [new issue](https://gitlab.com/artoale/joe/-/issues).
