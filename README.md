# Joe

This is a super minimal, **not** production ready python web framework trying to mimic Django.

Reference repo for "Joe: Build yourself a Django".

This repo should be taken purely as a learning exercise. Its intent **is not** to provide a functional
framework, but to learn more about Django (and about some advanced features of python).

## Ideas for topics

* basic HTTP request handling (no WSGI)
* Routing to views
* Middlewares
* Lazy settings (useful for further things later on)
* Models, Model fields and Database Backends (<- this clearly needs splitting)
* commands?
* migrations?
* multiple apps?
* wsgi?


### Note on the name Joe

Django (the framework) was named after Django Reinhardt, a jazz guitarist. Incidentally, it is
also the name of a fictional character in multiple spaghetti westerns. Joe is the (little-known)
name used for the character played by Clint Eastwood in a Fistful of dollars, also known as the
"Man with no name", "Manco" and "Blondie" throughout the trilogy.

## How to try this out

Currently there are no third-party dependencies, and the project has been tested with python 3.9

Clone the repo and run `python -m joe`

## Testing

Unit tests coming soon
