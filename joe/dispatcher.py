import logging
from functools import wraps
from importlib import import_module

from joe.conf import settings
from joe.exceptions import ImproperlyConfigured
from joe.request import HttpRequest
from joe.response import HttpResponse
from joe.urls import URLResolver, include

logger = logging.getLogger(__name__)


_NOT_FOUND_PAGE_CONTENT = """<!doctype html>
<html>
    <body>
        <h1>Joe says: Page not found</h1>
    </body>
<html>"""

_SERVER_ERROR_PAGE_CONTENT = """<!doctype html>
<html>
    <body>
        <h1>Joe says: Internal Server Error</h1>
    </body>
<html>"""


def exception_to_response(get_response):
    """Wraps a link in the middleware chain to return a broken HTTP response.

    Currenlty, we default to a 500. We could be smarter and have separate
    exceptions per http error (e.g. a view could do `raise Http404`) and
    convert it here to the correct response. We could also use this as a hook
    to provide debug information (e.g. tried routes for a 404, nicely printed
    exception, etc..)
    """

    @wraps(get_response)
    def inner(request):
        try:
            return get_response(request)
        except Exception:
            return HttpResponse(_SERVER_ERROR_PAGE_CONTENT, status_code=500)

    return inner


class Dispatcher:
    """Handles dispatching requests to the correct view

    Uses URLResolver to load url configs from settings.ROOT_URLCONF

    There is some minimal error handling:
        * a 404 is returned if no view is found
        * a 500 is returned if a view raises an exception
    """

    def __init__(self) -> None:
        self.root_resolver = URLResolver(r"^/", include(settings.ROOT_URLCONF))
        self._setup_middlewares()

    def _setup_middlewares(self) -> None:
        """Creates the middleware chain"""

        get_next = exception_to_response(self._do_dispatch)

        self._process_views_middlewares = []
        self._process_exceptions_middlewares = []

        for middleware_path in reversed(settings.MIDDLEWARE):
            module_path, module_prop = middleware_path.rsplit(".", 1)
            middleware_module = import_module(module_path)
            middleware = getattr(middleware_module, module_prop)

            get_next = middleware(get_next)
            if not callable(get_next):
                raise ImproperlyConfigured(
                    f"Middleware initialization '{middleware_path}' returned {get_next}"
                    " which is not a valid middleware (callable expected)."
                )

            if hasattr(get_next, "process_view"):
                # process_views are processed top-to-bottom
                self._process_views_middlewares.insert(0, get_next.process_view)

            if hasattr(get_next, "process_exception"):
                # process_exceptions are processed inside-out
                self._process_exceptions_middlewares.append(get_next.process_exception)

            get_next = exception_to_response(get_next)

        self._middleware_chain = get_next

    def _do_dispatch(self, request: HttpRequest):
        path = request.path
        response = None
        logger.debug("Got request for path %r", path)

        resolved = self.root_resolver.resolve(path)

        if not resolved:
            logger.debug("No route found for match")
            return HttpResponse(_NOT_FOUND_PAGE_CONTENT, status_code=404)

        try:
            view, kwargs = resolved
            for view_processor in self._process_views_middlewares:
                response = view_processor(request, view, kwargs)
                if response:
                    return response

            return view(request, **kwargs)
        except Exception as e:
            for exception_processor in self._process_exceptions_middlewares:
                response = exception_processor(request, e)

            if response:
                return response
            else:
                # _do_dispatch exception are handled by exceptions_to_response
                raise

    def dispatch(self, request: HttpRequest):
        return self._middleware_chain(request)

    def __repr__(self):
        return f"{self.__class__.__name__}()"
