DEBUG = False
DATABASES = {}

# Requests with many fields could be DoS attempts
DATA_UPLOAD_MAX_NUMBER_FIELDS = 1000

# Prevent reading arbitrarily large files (2.5 MiB)
DATA_UPLOAD_MAX_MEMORY_SIZE = 2.5 * 1024 * 1024

MIDDLEWARE = []
