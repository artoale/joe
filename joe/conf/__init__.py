"""
Implement django-like configuration
"""

import os
from importlib import import_module
from typing import Any

from joe.conf import global_settings

ENV_VARIABLE = "JOE_SETTINGS_MODULE"


class Settings:
    """
    Provides a  singleton object acting as a registry for user configuration

    Usage:

    ```
    from joe.conf import settings
    print(setting.DEBUG)
    ```
    """

    def __init__(self, settings_module):
        self.settings_module = settings_module

        # Load the module first, so that it blows quickly if misconfigured
        module = import_module(settings_module)

        for key in dir(global_settings):
            if key.isupper():
                setattr(self, key, getattr(global_settings, key))

        for key in dir(module):
            if key.isupper():
                value = getattr(module, key)
                # We could do some validation and checks here - e.g. warn if a
                # given setting is in the wrong format
                setattr(self, key, value)

    def __repr__(self):
        return f"{self.__class__.__name__}({self.settings_module!r})"


SETTINGS_MODULE_NAME = os.environ.get(ENV_VARIABLE)
if not SETTINGS_MODULE_NAME:
    raise RuntimeError(f"{ENV_VARIABLE} not configured")

settings: Any = Settings(SETTINGS_MODULE_NAME)
