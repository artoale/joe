from collections import UserDict
from functools import cached_property
from typing import BinaryIO
from urllib.parse import parse_qs

from joe.conf import settings


class HttpRequest:
    """Basic HTTP Request.

    Provides basic handling of query parameters, post data and content.
    """

    def __init__(
        self,
        method: str,
        path: str,
        headers,
        query_string: str,
        stream: BinaryIO,
    ):
        super().__init__()
        self.method = method
        self.path = path
        self.headers = headers

        try:
            self.encoding = headers.get_charsets()[0] or "utf-8"
        except IndexError:
            self.encoding = "utf-8"

        self._stream = stream
        self._query_string = query_string

    @cached_property
    def GET(self):
        return QueryDict(self._query_string)

    @cached_property
    def POST(self):
        if self.headers["Content-Type"] == "multipart/form-data":
            raise NotImplementedError("multipart/form-data")

        if self.headers["Content-Type"] == "application/x-www-form-urlencoded":
            query = self.body.decode(self.encoding)
            return QueryDict(query)

        return QueryDict()

    @cached_property
    def body(self):
        """Read the whole incoming stream

        TODO: provide a streaming API as well
        """
        try:
            to_read = int(self.headers["Content-Length"])
        except ValueError:
            to_read = 0
        if to_read > settings.DATA_UPLOAD_MAX_MEMORY_SIZE:
            raise OSError(f"Request body too large ({to_read} bytes).")

        # Let this raise
        return self._stream.read(to_read)

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__}: {self.method!r}, {self.path!r}>"


class QueryDict(UserDict):
    """Dictionary extension for supporting multiple values and querystrings."""

    def __init__(self, query_string=None):
        # Assuming query_string is already decoded here

        super().__init__()

        if query_string:
            query_data = parse_qs(
                query_string,
                keep_blank_values=True,
                max_num_fields=settings.DATA_UPLOAD_MAX_NUMBER_FIELDS,
            )
            self.data.update(query_data)

    def __getitem__(self, key):
        if key not in self.data:
            raise KeyError(key)

        try:
            return self.data[key][-1]
        except IndexError:
            return []

    def __setitem__(self, key, item):
        self.data[key] = [item]

    def getlist(self, key):
        """Return the list associated with key."""
        return self.data[key]

    def setlist(self, key, value):
        """Replace key with the provided list"""
        self.data[key] = value

    def appendlist(self, key, value):
        """Add value to the list key, creating it if not defined"""
        self.data.setdefault(key, [])
        self.data[key].append(value)

    def items(self):
        """Iterator that respects __getitem__ semantics

        Only returns last item of lists
        """
        return ((key, self[key]) for key in self.data.keys())

    def lists(self):
        """Iterator for (key, list) tuples"""
        return self.data.items()

    def __repr__(self):
        return f"<{self.__class__.__name__}: {self.data!r}>"
