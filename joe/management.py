import argparse
import logging
import os
import sys

logger = logging.getLogger(__name__)

host = "localhost"
port = 8000


def get_parser():
    prog_name = os.path.basename(sys.argv[0])

    if prog_name == "__main__.py":
        prog_name = "python -m joe"

    parser = argparse.ArgumentParser(prog=prog_name)

    parser.add_argument(
        "--settings",
        action="store",
        help="Sets the settings module.",
        default=None,
    )

    return parser


def execute_from_command_line(argv=None):
    argv = argv if argv is not None else sys.argv[1:]
    parser = get_parser()
    args = parser.parse_args(argv)

    # Configure logging (this will need to be based on settings or something)
    logging.basicConfig(
        style="{",
        format="[{name}] {message}",
        level=logging.INFO,
    )

    if args.settings:
        os.environ["JOE_SETTINGS_MODULE"] = args.settings

    # Since our settings are not lazy, import needs to happen here
    from joe.server import get_server

    logger.info("Starting http server")
    server = get_server(host, port)
    try:
        logger.info("Listening on %s:%d", host, port)
        server.serve_forever()
    except KeyboardInterrupt:
        logger.info("Received Keyboard interrupt, shutting down")
    except OSError as e:
        logger.error("Error while creating HTTP server %s", e)
        exit(1)
