import functools
import logging
from http.server import BaseHTTPRequestHandler, HTTPServer, ThreadingHTTPServer
from urllib.parse import urlparse

from joe import __version__
from joe.dispatcher import Dispatcher
from joe.request import HttpRequest

logger = logging.getLogger(__name__)


class RequestHandler(BaseHTTPRequestHandler):
    """Basic request handler

    This is an example server, a real framework would implement this using the
    WSGI protocol and delegate to a web server (like gunicorn) the handling of
    HTTP requests.
    """

    SUPPORTED_METHODS = ["GET", "POST", "PUT", "PATCH", "DELETE"]

    server_version = "joe/" + __version__

    def __init__(self, dispatcher: Dispatcher, *args, **kwargs) -> None:
        self._dispatcher = dispatcher
        super().__init__(*args, **kwargs)

    def __getattr__(self, name: str):
        if name.startswith("do_"):
            method = name[3:]
            if method in self.SUPPORTED_METHODS:
                return functools.partial(self._handle_request, method)

            raise AttributeError(f"HTTP verb '{method}' not supported")
        else:
            cls = type(self)
            raise AttributeError(f"'{cls.__name__!r}' object has no attribute {name!r}")

    def _handle_request(self, verb: str):
        url = urlparse(self.path)
        request = HttpRequest(
            verb,
            url.path,
            self.headers,
            query_string=url.query,
            stream=self.rfile,
        )

        # Delegate handling to the dispatcher.
        # Note, any error in the view is already handled by the dispatcher.
        response = self._dispatcher.dispatch(request)

        # First we send the status code...
        self.send_response(response.status_code)

        # ...then all the headers...
        for header, value in response.headers.items():
            self.send_header(header, value)

        # TODO:...we should also set cookies here, but we don't (yet)...
        self.end_headers()

        # ...and finally we write the content
        self.wfile.write(response.content)

    def log_message(self, format, *args):
        logger.info(format % args)


def get_server(host: str, port: int) -> HTTPServer:
    """Get a simple HTTP server"""
    address = (host, port)
    dispatcher = Dispatcher()

    handler = functools.partial(RequestHandler, dispatcher)

    server = ThreadingHTTPServer(address, handler)
    return server
