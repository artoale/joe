from copy import copy
from typing import MutableMapping, Optional, Union

_RESPONSE_REPR_TEMPLATE = """{0.__class__.__name__}(
    {0.content!r},
    status_code={0.status_code!r},
    content_type={0.content_type!r},
    charset={0.charset!r},
    headers={0.headers!r},
)"""


class HttpResponse:
    """Basic HTTP Response

    Handles some limited common cases, like sending back html by default
    """

    def __init__(
        self,
        content: Union[bytes, str] = b"",
        *,  # All subsequent params should be passed as named params
        status_code: int = 200,
        content_type: str = "text/html",
        charset: str = "utf-8",
        headers: Optional[MutableMapping[str, str]] = None,
    ):
        super().__init__()

        # just an attribute
        self.charset = charset

        # Setting headers first...since other property setters use them
        if headers is None:
            self.headers = {}
        else:
            self.headers = copy(headers)

        # Setting defaults for internals
        self._status_code = 200
        self._content_type = "utf-8"

        # These are properties, setting them last, setters may raise, let them
        self.content = content
        self.status_code = status_code
        self.content_type = content_type

    def __repr__(self) -> str:
        return _RESPONSE_REPR_TEMPLATE.format(self)

    @property
    def status_code(self) -> int:
        return self._status_code

    @status_code.setter
    def status_code(self, code):
        self._status_code = int(code)

    @property
    def content(self) -> bytes:
        return self._content

    @content.setter
    def content(self, val: Union[bytes, str]):
        """Setter for content

        Encodes the passed string if necessary, and sets the content-length header
        accordingly. The right way of doing this would be lazily on the getter,
        but it would make setting the headers automatically more complex.
        """
        if isinstance(val, bytes):
            self._content = val
        else:
            self._content = val.encode(self.charset)

        self.headers["Content-Length"] = str(len(self._content))

    @property
    def content_type(self) -> str:
        return self._content_type

    @content_type.setter
    def content_type(self, val: str):
        """Setter for content-type

        Also sets the Content-Type header accordingly in the format:
        Content-Type: <content_type>; charset=<charset>

        This doesn't really work unless you set the charset first, and as per
        content setter it should be done lazily.
        """
        self._content_type = val
        self.headers["Content-Type"] = f"{self._content_type}; charset={self.charset}"
