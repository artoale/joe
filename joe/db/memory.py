import pickle
import threading
from contextlib import contextmanager

from joe.exceptions import DatabaseError

from .base import BaseDatabase

_tables = {}
_locks = {}


def _lock_for_table(table_name: str):
    """Get or create a global threading lock for the given table_name"""
    return _locks.setdefault(table_name, threading.Lock())


@contextmanager
def _locked_table(table_name: str):
    """Utility function to get a "locked" table.

    Creates the table if one with the given table_name does not exist yet.
    """
    lock = _lock_for_table(table_name)
    with lock:
        yield _tables.setdefault(table_name, {})


class InMemoryDatabase(BaseDatabase):
    """Simple database backend that keeps entities in memory.

    Each "table" is locked independently, and entities are pickled/unpickled to
    support multiple thread accessing the same data.
    """

    def insert(self, model):
        """Add the provided model to the database.

        Raises a DatabaseError if the entity primary key is already present.
        If the model primary key has no value, it assigns one automatically.
        """
        table_name = model.__class__.__name__
        with _locked_table(table_name) as table:
            pk = model.pk
            if pk is None:
                pk = len(table) + 1

            if pk in table:
                raise DatabaseError(f"Duplicate primary key {pk}")

            model.pk = pk
            pickled_data = pickle.dumps(model)
            table[pk] = pickled_data

    def update(self, model):
        """Update the provided model in the database

        Raises DatabaseError if model does not have a primary key or if it is
        not present
        """
        table_name = model.__class__.__name__
        pk = model.pk
        if not pk:
            raise DatabaseError(f"Cannot update, {model} has no primary key")

        pickled_data = pickle.dumps(model)
        with _locked_table(table_name) as table:
            if pk not in table:
                raise DatabaseError(f"Model with pk {pk} not found")

            table[pk] = pickled_data

    def delete(self, model):
        pk = model.pk
        if not pk:
            raise DatabaseError(f"Model {model} has no primary key")

        with _locked_table(model.__class__.__name__) as table:
            if pk not in table:
                raise DatabaseError(f"Model with pk {pk} not found")

            del table[pk]

    def get(self, klass, pk):

        with _locked_table(klass.__name__) as table:
            if pk not in table:
                raise DatabaseError(f"Model with pk {pk} not found")

            pickled_data = table[pk]

        return pickle.loads(pickled_data)

    def query(self, query): ...

    def flush(self):
        for table_name in _tables.keys():
            with _locked_table(table_name) as table:
                table.clear()
