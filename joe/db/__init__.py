"""
Provides access to database connection

Usage example

```
from joe.db import databases

databases['default'].insert(model_instance)
```

This is usually called by the model's save method
"""

from importlib import import_module
from threading import local

from joe.conf import settings
from joe.exceptions import ImproperlyConfigured

DATABASE_SETTINGS = "DATABASES"


class DatabaseHandler:
    """
    Registry for thread-local database connections
    """

    def __init__(self):
        self._databases = local()

    def _load_backend(self, backend_str):
        try:
            module_path, module_prop = backend_str.rsplit(".", 1)
            backend_mod = import_module(module_path)
            return getattr(backend_mod, module_prop)
        except (ModuleNotFoundError, AttributeError) as m:
            raise ImproperlyConfigured(
                f"Database backend '{backend_str}' not found"
            ) from m

    def __getitem__(self, alias):
        if hasattr(self._databases, alias):
            return getattr(self._databases, alias)

        try:
            config = getattr(settings, DATABASE_SETTINGS)[alias]
        except KeyError as e:
            raise ImproperlyConfigured(f"Database '{alias}' does not exists") from e

        backend_str = config["ENGINE"]
        backend_cls = self._load_backend(backend_str)
        backend = backend_cls(config, alias)
        setattr(self._databases, alias, backend)
        return backend

    def __setitem__(self, alias, value):
        setattr(self._databases, alias, value)


databases = DatabaseHandler()
