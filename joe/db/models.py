import logging

from joe.db import databases
from joe.exceptions import ImproperlyConfigured, ValidationError

logger = logging.getLogger(__name__)


class Field:
    def __init__(self, primary_key=False, default=None):
        self.default = default
        self.primary_key = primary_key

    def __set_name__(self, owner, name):
        self.name = name
        self.model = owner

    @property
    def full_name(self):
        return f"{self.model.__name__}.{self.name}"

    def clean(self, value):
        """Convert to the correct python object and validates it."""
        value = self.to_python(value)
        self._validate_required(value)
        self.validate(value)
        return value

    def to_python(self, value):
        """Convert to the correct python object."""
        return value

    def _validate_required(self, value):
        if value is None:
            raise ValidationError(f"Field '{self.full_name}' cannot be null")

    def validate(self, value):
        pass


class CharField(Field):
    def __init__(self, max_length=None, **kwargs):
        super().__init__(**kwargs)
        self.max_length = max_length

    def to_python(self, value):
        if value is None or isinstance(value, str):
            return value
        return str(value)

    def validate(self, value):
        if self.max_length is not None and len(value) > self.max_length:
            raise ValidationError(
                f"Field '{self.full_name}' is too long (max_length={self.max_length})"
            )


class IntegerField(Field):
    def to_python(self, value):
        if value is None or isinstance(value, int):
            return value
        try:
            return int(value)
        except (ValueError, TypeError) as e:
            raise ValidationError(
                f"Field '{self.full_name}' is not an integer (got '{value}')"
            ) from e


class AutoField(IntegerField):
    def __init__(self, **kwargs):
        kwargs["primary_key"] = True
        super().__init__(**kwargs)

    def clean(self, value):
        # Only convert to int if necessary, skip actual validation
        return self.to_python(value)


class Model:
    _fields: dict[str, Field]
    _pk_field: Field

    @property
    def pk(self):
        return getattr(self, self._pk_field.name)

    @pk.setter
    def pk(self, value):
        setattr(self, self._pk_field.name, value)

    def __init__(self, **kwargs):
        for name, field in self._fields.items():
            arg = kwargs.pop(name, field.default)
            setattr(self, name, arg)
        if kwargs:
            raise TypeError(f"Invalid argument(s): {','.join(kwargs)}")

        self._adding = True

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        cls._fields = {}

        # Use a local var to avoid changing __dict__ before iteration is done
        pk_field = None
        for name, field in cls.__dict__.items():
            if isinstance(field, Field):
                cls._fields[name] = field
                if field.primary_key:
                    if pk_field:
                        raise ImproperlyConfigured(
                            "Multiple primary keys are not allowed"
                        )
                    else:
                        pk_field = field

        if pk_field is None:
            pk_field = AutoField()
            cls._fields["id"] = pk_field
            pk_field.__set_name__(cls, "id")

        cls._pk_field = pk_field

    def clean_fields(self):
        for name, field in self._fields.items():
            value = getattr(self, name)
            setattr(self, name, field.clean(value))

    def full_clean(self):
        self.clean_fields()
        self.clean()

    def clean(self):
        """Hook for custom validation."""
        pass

    @classmethod
    def by_pk(cls, pk, using="default"):
        db = databases[using]
        entity = db.get(cls, pk)
        entity._adding = False
        return entity

    def save(self, using="default"):
        db = databases[using]
        operation = db.update

        if self._adding:
            operation = db.insert

            if not isinstance(self.__class__._pk_field, AutoField) and self.pk is None:
                raise ValueError("Cannot insert model without a primary key")

        operation(self)
