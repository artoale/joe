"""
Starts joe as a server when run as a module (e.g. `python -m joe`)
"""
from joe.management import execute_from_command_line

execute_from_command_line()
