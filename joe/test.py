from contextlib import contextmanager

from joe.conf import settings

# Sentinel value, when overriding an unset setting
_unset_sentinel = object()


@contextmanager
def override_settings(**kwargs):
    """Override a setting value, intended use in tests only.

    Use as context manager or function decorator:

    ```
    with override_settings(X=1):
        assert settings.X == 1

    # settings.X == previous value
    ```
    """
    oldvals = {}
    for key in kwargs.keys():
        if hasattr(settings, key):
            oldvals[key] = getattr(settings, key)
        else:
            oldvals[key] = _unset_sentinel

    try:
        for key, val in kwargs.items():
            setattr(settings, key, val)
        yield
    finally:
        for key, val in oldvals.items():
            if val == _unset_sentinel:
                delattr(settings, key)
            else:
                setattr(settings, key, val)
