"""
Global exceptions for Joe
"""


class ImproperlyConfigured(Exception):
    """Joe is not configured correctly"""


class ValidationError(Exception):
    """Error while validating a field"""


class DatabaseError(Exception):
    """Error while using the database"""


class IntegrityError(DatabaseError):
    """Integrity violation when using the database"""
