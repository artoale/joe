import logging
import re
from importlib import import_module
from typing import Callable, Optional, Union

logger = logging.getLogger(__name__)


class URLPattern:
    def __init__(
        self,
        pattern: str,
        view: Callable,
        extra_kwargs: Optional[dict] = None,
    ):
        self.pattern = pattern
        self.view = view
        self.regex = re.compile(pattern)
        self.extra_kwargs = {} if extra_kwargs is None else extra_kwargs

    def resolve(self, path):
        match = self.regex.search(path)

        if match:
            kwargs = {**match.groupdict(), **self.extra_kwargs}
            return (self.view, kwargs)

    def __repr__(self):
        return f"<{self.__class__.__name__} {self.pattern!r}>"


class URLResolver:
    def __init__(
        self,
        pattern: str,
        urlpatterns: Union[list, tuple],
        extra_kwargs: Optional[dict] = None,
    ):
        self.pattern = pattern
        self.urlpatterns = urlpatterns
        self.regex = re.compile(pattern)
        self.extra_kwargs = {} if extra_kwargs is None else extra_kwargs

    def resolve(self, path):
        match = self.regex.search(path)

        if match is None:
            # TODO: we could raise a custom error here to keep track of
            # tried routes
            return

        kwargs = match.groupdict()
        end = match.end()
        sub_path = path[end:]

        for pattern in self.urlpatterns:
            sub_match = pattern.resolve(sub_path)

            if sub_match:
                view, sub_kwargs = sub_match
                all_kwargs = {**kwargs, **self.extra_kwargs, **sub_kwargs}
                return (view, all_kwargs)

            # TODO: track tried routes here

    def __repr__(self):
        return f"<{self.__class__.__name__} {self.pattern!r}>"


def re_path(route: str, view_or_include, kwargs=None):
    if callable(view_or_include):
        return URLPattern(route, view_or_include, extra_kwargs=kwargs)

    else:
        return URLResolver(route, view_or_include, extra_kwargs=kwargs)


def include(urlpatterns):
    """Loads urlpattern

    If a string is provided it must be the dotted path to a python module
    that provides a "urlpatterns" attribute.
    If passed as an iterable, it behaves as an identity function
    """

    if isinstance(urlpatterns, str):
        module = import_module(urlpatterns)
        return module.urlpatterns

    if isinstance(urlpatterns, (tuple, list)):
        return urlpatterns

    raise TypeError(
        f"Argument must be a string, list or tuple. {type(urlpatterns)} provided."
    )
