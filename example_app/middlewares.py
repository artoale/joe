import json
import logging

logger = logging.getLogger(__name__)


def json_decoder(get_response):
    """
    Middleware for decoding json-encoded requests
    """

    def middleware(request):
        if request.path.startswith("/api/") and request.method in (
            "POST",
            "PUT",
            "PATCH",
        ):
            try:
                body = request.body.decode(request.encoding)
                json_payload = json.loads(body)
                request.json = json_payload
            except json.JSONDecodeError:
                logger.warning("Failed to parse json body")

        return get_response(request)

    return middleware


def extra_logging(get_response):
    """
    Baisc middleware showing the "process view" functionality
    """

    def middleware(request):
        return get_response(request)

    def process_view(request, view_fn, view_kwargs):
        if getattr(view_fn, "extra_logging", False):
            logger.info("About to call the view '%s'", view_fn.__name__)

    middleware.process_view = process_view

    return middleware
