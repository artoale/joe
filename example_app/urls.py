"""
example_app root url configuration
"""

from example_app import views
from joe.urls import re_path

urlpatterns = [
    re_path(r"^$", views.home),
    re_path(r"^details/(?P<details>[\w-]+)/$", views.details),
    re_path(r"^error/$", views.error),
    re_path(r"^api/$", views.api),
    re_path(r"^api/cars/$", views.cars),
    re_path(r"^api/cars/(?P<car_id>\d+)$", views.car_details),
]
