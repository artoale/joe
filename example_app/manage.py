#!/usr/bin/env python
"""
Executable python script to start joe with the necessary settings
"""
import os
import sys

BASE_DIR = os.path.abspath(os.path.join((os.path.dirname(__file__)), ".."))
sys.path.append(BASE_DIR)

if __name__ == "__main__":
    from joe.management import execute_from_command_line

    os.environ.setdefault("JOE_SETTINGS_MODULE", "example_app.settings")
    execute_from_command_line()
