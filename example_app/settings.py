ROOT_URLCONF = "example_app.urls"

MIDDLEWARE = [
    "example_app.middlewares.json_decoder",
    "example_app.middlewares.extra_logging",
]

DATABASES = {
    "default": {
        "ENGINE": "joe.db.memory.InMemoryDatabase",
        "OPTIONS": {},
    }
}
