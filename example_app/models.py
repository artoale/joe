from joe.db.models import CharField, IntegerField, Model


class Car(Model):
    model = CharField(max_length=10)
    make = CharField(max_length=10)
    plate = CharField(max_length=7)
    mileage = IntegerField()
    sound = CharField(default="Honk!")

    def clean(self):
        if self.make == "Fiat" and self.model not in (
            "Punto",
            "500",
        ):
            raise ValueError(f"'{self.model}' is not made by '{self.make}'")
        if self.make == "Ford" and self.model not in (
            "Focus",
            "Bronco",
        ):
            raise ValueError(f"'{self.model}' is not made by '{self.make}'")


# punto = Car(
#     model="Punto",
#     plate="ABCD",
#     mileage=100_000,
# )

# # punto.clean_fields()  # Raises ValidationError("Field 'make' is required")

# punto.make = "Fiat"
# punto.clean_fields()  # Does not raise

# punto.make = "A long make name that exceeds max_length"
# # punto.clean_fields()  # Raises ValidationError("Field 'make' is too long (max_length=10)")

# punto.make = "Ford"
# punto.clean_fields()  # Does not raise, model.clean is not called by clean_fields()
# # punto.full_clean()  # Raises ValidationError("'Punto' is not made by 'Ford'")

# punto.make = "Fiat"
# punto.full_clean()  # Does not raise

# punto.mileage = "not_a_number"
# # punto.clean_fields()  # Raises ValidationError("'not_a_number' is not an integer")

# punto.mileage = "1234"
# punto.clean_fields()  # Does not raise, '1234' is converted to 1234 with int()
# assert punto.mileage == 1234  # True
