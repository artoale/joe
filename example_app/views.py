"""
Joe example app views
"""

import json
import logging

from example_app.models import Car
from joe.response import HttpResponse

logger = logging.getLogger(__name__)


def home(req):
    """Simple view providing the most basic access"""
    logger.info("Home called")
    return HttpResponse("Welcome to the example app!")


def details(req, details):
    """Simple view showing middleware interaction"""
    logger.info("Details called")
    return HttpResponse(f"Details: {details}")


# see extra_logging middleware
details.extra_logging = True


def error(req):
    """
    View that always raises.
    Shows how joe deals with exceptions in views
    """
    logger.info("Errors called")
    raise Exception("I am erroring")


def api(req):
    """
    View that shows how a middleware can provide json parsing
    """
    if req.method == "POST":
        logger.info("API POST received with %r", req.json)
        return HttpResponse('{"status": "ok"}', content_type="application/json")
    return HttpResponse('{"status": "meh"}', content_type="application/json")


def cars(req):
    """
    POST to this view to create a new car
    """
    if req.method != "POST":
        raise Exception("method not supported")

    try:
        new_car = Car(
            model=req.json["model"],
            make=req.json["make"],
            plate=req.json["plate"],
            mileage=req.json["mileage"],
        )
        new_car.full_clean()
        new_car.save()
    except Exception as e:
        logger.error(e)
        return HttpResponse(
            '{"error": "database error"}',
            status_code=500,
            content_type="application/json",
        )

    return HttpResponse(
        json.dumps(
            {
                "id": new_car.pk,
                "model": new_car.model,
                "plate": new_car.plate,
                "mileage": new_car.mileage,
                "make": new_car.make,
                "sound": new_car.sound,
            }
        ),
        status_code=200,
        content_type="application/json",
    )


def car_details(req, car_id):
    """
    Retrieves details of a Car model, by pk
    """
    try:
        car = Car.by_pk(int(car_id))
    except Exception as e:
        logger.error(e)
        return HttpResponse(
            '{"error": "Car with id %s not found"}' % car_id,
            status_code=500,
            content_type="application/json",
        )

    return HttpResponse(
        json.dumps(
            {
                "id": car.pk,
                "model": car.model,
                "plate": car.plate,
                "mileage": car.mileage,
                "make": car.make,
                "sound": car.sound,
            }
        ),
        status_code=200,
        content_type="application/json",
    )
