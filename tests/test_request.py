from email.message import Message
from io import BytesIO

import pytest

from joe.request import QueryDict
from joe.test import override_settings
from tests.factories import request_factory


class TestHttpRequest:
    def test_query_string(self):
        request = request_factory(query_string="a=1&b=2&c=3")
        assert request.GET["a"] == "1"
        assert request.GET["b"] == "2"
        assert request.GET["c"] == "3"

    def test_body_urlencoded(self):
        bytes_content = "a=1&b=2&c=3".encode("utf-8")
        stream = BytesIO(bytes_content)
        headers = Message()
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        headers["Content-Length"] = str(len(bytes_content))
        request = request_factory(stream=stream, headers=headers)

        assert request.POST["a"] == "1"
        assert request.POST["b"] == "2"
        assert request.POST["c"] == "3"

    def test_body_raw(self):
        bytes_content = "ᴨ is ≈ 3.14".encode("utf-8")
        stream = BytesIO(bytes_content)
        headers = Message()
        headers["Content-Type"] = "application/json"
        headers["Content-Length"] = str(len(bytes_content))
        request = request_factory(stream=stream, headers=headers)

        assert request.body == bytes_content


class TestQueryDict:
    def test_parse_single(self):
        qd = QueryDict("a=1&b=2&c=3")
        assert qd["a"] == "1"
        assert qd["b"] == "2"
        assert qd["c"] == "3"

        qd["x"] = "4"
        assert qd["x"] == "4"

        with pytest.raises(KeyError):
            qd["y"]

        items = [val for val in qd.items()]
        assert items == [
            ("a", "1"),
            ("b", "2"),
            ("c", "3"),
            ("x", "4"),
        ]

        lists = [val for val in qd.lists()]
        assert lists == [
            ("a", ["1"]),
            ("b", ["2"]),
            ("c", ["3"]),
            ("x", ["4"]),
        ]

        qd.setlist("y", [])

        assert qd["y"] == []

    def test_multiple(self):
        qd = QueryDict("a=1&a=2&a=3")
        assert qd["a"] == "3"
        assert qd.getlist("a") == ["1", "2", "3"]

        qd["a"] = "4"
        assert qd["a"] == "4"
        assert qd.getlist("a") == ["4"]

        qd.appendlist("a", "5")
        assert qd.getlist("a") == ["4", "5"]

        items = [val for val in qd.items()]
        assert items == [
            ("a", "5"),
        ]

        lists = [val for val in qd.lists()]
        assert lists == [
            ("a", ["4", "5"]),
        ]

    @override_settings(DATA_UPLOAD_MAX_NUMBER_FIELDS=10)
    def test_limit(self):
        query_string = "a=1&" * 10 + "b=2"
        with pytest.raises(ValueError):
            QueryDict(query_string)
