import os


def pytest_sessionstart(session):
    os.environ.setdefault("JOE_SETTINGS_MODULE", "example_app.settings")
