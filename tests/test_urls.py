from joe.urls import URLPattern, URLResolver, include, re_path


class TestURLPattern:
    def test_resolves_paths(self):
        def fake_view():
            pass

        pattern = URLPattern(r"^/abc/d/$", fake_view)

        match = pattern.resolve("/abc/d/")
        assert match is not None
        matched_view, matched_kwargs = match
        assert matched_kwargs == {}
        assert matched_view == fake_view

    def test_return_none_if_missing(self):
        def fake_view():
            pass

        pattern = URLPattern(r"^/abc/d/$", fake_view)

        match = pattern.resolve("/abc/")
        assert match is None

    def test_return_correct_url_params(self):
        def fake_view():
            pass

        pattern = URLPattern(r"^/abc/(?P<argument>\d+)/$", fake_view)

        match = pattern.resolve("/abc/123/")
        assert match is not None
        matched_view, matched_kwargs = match
        assert matched_view == fake_view
        assert matched_kwargs == {"argument": "123"}

    def test_return_mixed_params(self):
        def fake_view():
            pass

        pattern = URLPattern(
            r"^/abc/(?P<argument>\d+)/$",
            fake_view,
            extra_kwargs={"argument_2": "abc"},
        )

        match = pattern.resolve("/abc/123/")
        assert match is not None
        matched_view, matched_kwargs = match
        assert matched_view == fake_view
        assert matched_kwargs == {"argument": "123", "argument_2": "abc"}


class TestURLResolver:
    def test_resolves_flat_structures(self):
        def home():
            pass

        def articles():
            pass

        def drafts():
            pass

        urlconf = [
            re_path(r"^$", home),
            re_path(r"^articles/(?P<article_id>\w+)/$", articles),
            re_path(r"^drafts/(?P<article_id>\w+)/$", drafts, kwargs={"draft": True}),
        ]

        resolver = URLResolver(r"^/", urlconf)

        match = resolver.resolve("/")
        assert match is not None
        assert match[0] is home

        match = resolver.resolve("/articles/introducing_joe/")
        assert match is not None
        assert match[0] is articles
        assert match[1] == {"article_id": "introducing_joe"}

        match = resolver.resolve("/drafts/joe_is_super_cool/")
        assert match is not None
        assert match[0] is drafts
        assert match[1] == {"article_id": "joe_is_super_cool", "draft": True}

    def test_resolves_nested_structures(self):
        def home():
            pass

        def articles():
            pass

        def drafts():
            pass

        nested_urlconf = [
            re_path(r"^articles/(?P<article_id>\d+)/$", articles),
            re_path(r"^drafts/(?P<article_id>\d+)/$", drafts, kwargs={"draft": True}),
        ]

        urlconf = [
            re_path(r"^$", home),
            re_path(r"^api/v1/", include(nested_urlconf)),
        ]

        resolver = URLResolver(r"^/", urlconf)

        match = resolver.resolve("/")
        assert match is not None
        assert match[0] is home

        match = resolver.resolve("/api/v1/articles/123/")
        assert match is not None
        assert match[0] is articles
        assert match[1] == {"article_id": "123"}

        match = resolver.resolve("/api/v1/drafts/456/")
        assert match is not None
        assert match[0] is drafts
        assert match[1] == {"article_id": "456", "draft": True}

    def test_resolves_nested_structures_with_imported_module(self):
        def home():
            pass

        urlconf = [
            re_path(r"^$", home),
            re_path(r"^api/v1/", include("tests.urlpatterns_test_module")),
        ]

        resolver = URLResolver(r"^/", urlconf)

        from .urlpatterns_test_module import articles, drafts

        match = resolver.resolve("/")
        assert match is not None
        assert match[0] is home

        match = resolver.resolve("/api/v1/articles/123/")
        assert match is not None
        assert match[0] is articles
        assert match[1] == {"article_id": "123"}

        match = resolver.resolve("/api/v1/drafts/456/")
        assert match is not None
        assert match[0] is drafts
        assert match[1] == {"article_id": "456", "draft": True}
