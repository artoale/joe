from joe.conf import settings
from joe.test import override_settings


def test_override_settings():
    assert settings.DEBUG is False
    with override_settings(DEBUG=True):
        assert settings.DEBUG is True
    assert settings.DEBUG is False

    try:
        with override_settings(DEBUG=True):
            raise Exception("FATALITY!")
    except Exception:
        assert settings.DEBUG is False
