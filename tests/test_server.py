import threading
from http.server import HTTPServer

import httpx
import pytest

from joe.server import get_server

_TEST_SERVER_PORT = 8095
_TEST_SERVER_HOST = "localhost"
_TEST_SERVER_TIMEOUT = 2


@pytest.fixture(scope="module", autouse=True)
def live_server():
    def start_server(server: HTTPServer):
        server.serve_forever()

    server = get_server(_TEST_SERVER_HOST, _TEST_SERVER_PORT)
    thread = threading.Thread(target=start_server, args=[server])
    thread.start()
    yield server
    server.shutdown()
    thread.join(timeout=_TEST_SERVER_TIMEOUT)
    if thread.is_alive():
        raise RuntimeError("Test server did not shutdown in time")


@pytest.fixture
def client() -> httpx.Client:
    return httpx.Client(base_url=f"http://{_TEST_SERVER_HOST}:{_TEST_SERVER_PORT}")


def test_get_successful(client: httpx.Client):
    response = client.get("/")
    assert response.status_code == 200


def test_option_not_implemented(client: httpx.Client):
    response = client.options("/")
    assert response.status_code == 501


def test_post_successful(client: httpx.Client):
    response = client.post("/")
    assert response.status_code == 200


def test_put_successful(client: httpx.Client):
    response = client.put("/")
    assert response.status_code == 200


def test_patch_successful(client: httpx.Client):
    response = client.patch("/")
    assert response.status_code == 200


def test_get_server_error(client: httpx.Client):
    response = client.get("/error/")
    assert response.status_code == 500


def test_get_client_error(client: httpx.Client):
    response = client.get("/nonexistant")
    assert response.status_code == 404
