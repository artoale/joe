from unittest import mock

import pytest

from joe.dispatcher import Dispatcher
from joe.exceptions import ImproperlyConfigured
from joe.test import override_settings
from tests.factories import request_factory


@pytest.fixture()
def default_dispatcher():
    return Dispatcher()


def test_returns_500_for_error(default_dispatcher: Dispatcher):
    request = request_factory(path="/error/")

    response = default_dispatcher.dispatch(request)
    assert response.status_code == 500


def test_returns_404_for_not_found(default_dispatcher: Dispatcher):
    request = request_factory(path="/non-existing-route")

    response = default_dispatcher.dispatch(request)
    assert response.status_code == 404


@pytest.mark.parametrize("method", ["GET", "PUT", "POST", "DELETE"])
def test_returns_200_for_allowed_methods(default_dispatcher: Dispatcher, method: str):
    request = request_factory(method=method, path="/")

    response = default_dispatcher.dispatch(request)
    assert response.status_code == 200


@override_settings(
    MIDDLEWARE=[
        "tests.middleware_tests_module.middleware_1",
        "tests.middleware_tests_module.middleware_2",
    ]
)
def test_middlewares_ordered_correctly(monkeypatch):
    request = request_factory()
    manager = mock.Mock()
    before = mock.Mock()
    after = mock.Mock()
    manager.attach_mock(before, "before")
    manager.attach_mock(after, "after")

    monkeypatch.setattr("tests.middleware_tests_module.before_view", before)
    monkeypatch.setattr("tests.middleware_tests_module.after_view", after)

    # Need to use the non-default dispatcher, otherwise _setup_middleware would
    # be called before override_settings has time to affect anything
    dispatcher = Dispatcher()
    dispatcher.dispatch(request)

    expected_calls = [
        mock.call.before(1),
        mock.call.before(2),
        mock.call.after(2),
        mock.call.after(1),
    ]
    print(manager.mock_calls)

    assert manager.mock_calls == expected_calls


@override_settings(
    MIDDLEWARE=[
        "tests.middleware_tests_module.middleware_1",
        "tests.middleware_tests_module.middleware_bypass",
        "tests.middleware_tests_module.middleware_2",
    ]
)
def test_middlewares_can_bypass_views(monkeypatch):
    request = request_factory()
    manager = mock.Mock()
    before = mock.Mock()
    after = mock.Mock()
    view = mock.Mock()
    manager.attach_mock(before, "before")
    manager.attach_mock(after, "after")
    manager.attach_mock(view, "view")

    monkeypatch.setattr("tests.middleware_tests_module.before_view", before)
    monkeypatch.setattr("tests.middleware_tests_module.after_view", after)
    monkeypatch.setattr("example_app.views.home", view)

    dispatcher = Dispatcher()
    response = dispatcher.dispatch(request)

    assert response.status_code == 418

    expected_calls = [
        mock.call.before(1),
        mock.call.before("bypass"),
        mock.call.after(1),
    ]
    assert manager.mock_calls == expected_calls


@override_settings(
    MIDDLEWARE=[
        "tests.middleware_tests_module.middleware_raising",
    ]
)
def test_middlewares_can_throw():
    request = request_factory()
    dispatcher = Dispatcher()

    response = dispatcher.dispatch(request)

    assert response.status_code == 500


@override_settings(
    MIDDLEWARE=[
        "tests.middleware_tests_module.middleware_returns_none",
    ]
)
def test_middleware_must_return_callable():
    with pytest.raises(ImproperlyConfigured):
        Dispatcher()


@override_settings(
    MIDDLEWARE=[
        "tests.middleware_tests_module.MiddlewareClass",
    ]
)
def test_middleware_works_with_callable_class():
    request = request_factory()
    dispatcher = Dispatcher()

    response = dispatcher.dispatch(request)

    # MiddlewareClass adds a _with_class to the response
    assert getattr(response, "_with_class") is True


@override_settings(
    MIDDLEWARE=[
        "tests.middleware_tests_module.WithExceptionHandling",
    ]
)
def test_process_exception():
    request = request_factory(path="/error/")
    dispatcher = Dispatcher()

    response = dispatcher.dispatch(request)

    # WithExceptionHandling changes return status code
    assert response.status_code == 418


@override_settings(
    MIDDLEWARE=[
        "tests.middleware_tests_module.WithProcessView",
    ]
)
def test_process_view():
    request = request_factory(path="/")
    dispatcher = Dispatcher()

    response = dispatcher.dispatch(request)

    # WithExceptionHandling changes return status code
    assert response.status_code == 418
