from unittest import mock

from joe.management import execute_from_command_line, get_parser


def test_parser_settings():
    parser = get_parser()

    result = parser.parse_args(["--settings=ABC.D"])

    assert result.settings == "ABC.D"

    result = parser.parse_args(["--settings", "ABC.D"])

    assert result.settings == "ABC.D"


def test_execute_command_line_starts_server(monkeypatch):
    fake_server = mock.MagicMock()

    monkeypatch.setattr("joe.server.get_server", lambda *args: fake_server)
    execute_from_command_line([])

    assert fake_server.serve_forever.called
