import pytest

from joe.db.models import CharField, Field, IntegerField, Model
from joe.exceptions import ImproperlyConfigured, ValidationError


class TestWithField:
    def test_default_at_init(self):
        class WithDefault(Model):
            a = Field(default="a")
            b = Field(default="b")

        instance = WithDefault()
        assert instance.a == "a"
        assert instance.b == "b"

        instance = WithDefault(a="A", b="B")
        assert instance.a == "A"
        assert instance.b == "B"

    def test_fields_dict(self):
        class WithFields(Model):
            a = Field()
            b = Field()

        assert WithFields._fields["a"] == WithFields.a
        assert WithFields._fields["b"] == WithFields.b

    def test_field_has_name(self):
        class WithName(Model):
            a = Field()

        assert WithName._fields["a"].full_name == "WithName.a"

    def test_field_clean_uses_value(self):
        class AlwaysCleanField(Field):
            def clean(self, value):
                return "cleaned"

        class WithClean(Model):
            a = AlwaysCleanField()

        instance = WithClean(a="not cleaned")
        instance.clean_fields()
        assert instance.a == "cleaned"

    def test_field_clean_uses_value_full_clean(self):
        class AlwaysCleanField(Field):
            def clean(self, value):
                return "cleaned"

        class WithClean(Model):
            a = AlwaysCleanField()

        instance = WithClean(a="not cleaned")
        instance.full_clean()
        assert instance.a == "cleaned"

    def test_full_clean_calls_clean(self):
        class WithClean(Model):
            a = Field()

            def clean(self):
                self.a = "cleaned"

        instance = WithClean(a="not cleaned")
        instance.full_clean()
        assert instance.a == "cleaned"

    def test_validates_required_fields(self):
        class WithRequired(Model):
            a = Field()

        instance = WithRequired()
        with pytest.raises(ValidationError):
            instance.clean_fields()

    def test_validates_required_fields_full_clean(self):
        class WithRequired(Model):
            a = Field()

        instance = WithRequired()
        with pytest.raises(ValidationError):
            instance.full_clean()

    def test_validates_with_defaults(self):
        class WithDefaults(Model):
            a = Field(default="a")

        instance = WithDefaults()
        instance.a = None

        with pytest.raises(ValidationError):
            instance.clean_fields()

    def test_validates_with_defaults_full_clean(self):
        class WithDefaults(Model):
            a = Field(default="a")

        instance = WithDefaults()
        instance.a = None

        with pytest.raises(ValidationError):
            instance.full_clean()


class TestCharField:
    def test_max_length(self):
        class WithCharField(Model):
            a = CharField(max_length=2)

        instance = WithCharField(a="a")
        instance.clean_fields()

        instance.a = "abc"

        with pytest.raises(ValidationError):
            instance.clean_fields()

    def test_converts_to_str(self):
        class NotAString:
            def __str__(self):
                return "not a string"

        class WithCharField(Model):
            a = CharField()

        instance = WithCharField(a=NotAString())
        instance.clean_fields()
        assert instance.a == "not a string"


class TestIntegerField:
    def test_converts_to_int(self):
        class WithIntegerField(Model):
            a = IntegerField()

        instance = WithIntegerField(a="123")
        instance.clean_fields()
        assert instance.a == 123

    def test_converts_to_int_full_clean(self):
        class WithIntegerField(Model):
            a = IntegerField()

        instance = WithIntegerField(a="123")
        instance.full_clean()
        assert instance.a == 123

    def test_raises_on_invalid_int(self):
        class WithIntegerField(Model):
            a = IntegerField()

        instance = WithIntegerField(a="not an int")
        with pytest.raises(ValidationError):
            instance.clean_fields()


class TestPrimaryKey:
    def test_added_if_not_present(self):
        class WithAutoField(Model):
            name = CharField()

        assert "id" in WithAutoField._fields
        assert WithAutoField._fields["id"].primary_key is True

    def test_not_added_if_pk_specified(self):
        class WithoutAutofield(Model):
            email = CharField(primary_key=True)

        assert "id" not in WithoutAutofield._fields
        assert WithoutAutofield._fields["email"].primary_key is True

    def test_raises_if_multiple_pks(self):
        with pytest.raises(ImproperlyConfigured):

            class WithoutAutofield(Model):
                email = CharField(primary_key=True)
                name = CharField(primary_key=True)

    def test_pk_property_getter(self):
        class WithAutoField(Model):
            name = CharField(primary_key=True)

        instance = WithAutoField(name="name")
        assert instance.pk == "name"

    def test_pk_property_setter(self):
        class WithAutoField(Model):
            name = CharField(primary_key=True)

        instance = WithAutoField(name="name")
        instance.pk = "different"

        assert instance.name == "different"
