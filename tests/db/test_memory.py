"""Test module for InMemoryDatabase"""

import pytest

from joe.db import models
from joe.db.memory import InMemoryDatabase
from joe.exceptions import DatabaseError


@pytest.fixture()
def db():
    in_memory_db = InMemoryDatabase({}, "alias")
    yield in_memory_db
    in_memory_db.flush()


class ModelWithPK(models.Model):
    custom_pk = models.CharField(primary_key=True)
    name = models.CharField()


class ModelWithAutoPK(models.Model):
    name = models.CharField()


@pytest.fixture()
def model_with_pk(db):
    new = ModelWithPK(custom_pk="123", name="123")
    db.insert(new)
    return new


@pytest.fixture()
def model_with_auto_pk(db):
    new = ModelWithAutoPK(name="123")
    db.insert(new)
    return new


def test_insert_with_custom_pk(db):
    model_with_pk = ModelWithPK(custom_pk="123", name="123")

    db.insert(model_with_pk)

    from_db = db.get(ModelWithPK, "123")
    assert from_db.name == model_with_pk.name
    assert from_db.pk == model_with_pk.pk


def test_insert_with_same_custom_pk_raises(db):
    model_with_pk = ModelWithPK(custom_pk="123", name="123")
    db.insert(model_with_pk)
    model_with_pk_2 = ModelWithPK(custom_pk="123", name="456")

    with pytest.raises(DatabaseError):
        db.insert(model_with_pk_2)


def test_insert_with_auto_pk_adds_pk(db):
    model_with_pk = ModelWithAutoPK(name="123")
    db.insert(model_with_pk)

    assert model_with_pk.pk == 1


def test_insert_same_model_twice_raises(db):
    model_with_pk = ModelWithAutoPK(name="123")
    db.insert(model_with_pk)

    with pytest.raises(DatabaseError):
        db.insert(model_with_pk)


def test_get_ignores_future_changes(db):
    model_with_pk = ModelWithPK(custom_pk="123", name="123")
    db.insert(model_with_pk)

    model_with_pk.name = "456"
    from_db = db.get(ModelWithPK, "123")

    assert from_db != model_with_pk
    assert from_db.name == "123"
    assert model_with_pk.name == "456"


def test_update_with_custom_pk_stores_the_value(db, model_with_pk):
    model_with_pk.name = "567"
    db.update(model_with_pk)

    from_db = db.get(ModelWithPK, model_with_pk.pk)

    assert from_db.name == "567"


def test_update_with_auto_pk_stores_the_value(db, model_with_auto_pk):
    model_with_auto_pk.name = "567"
    db.update(model_with_auto_pk)

    from_db = db.get(ModelWithAutoPK, model_with_auto_pk.pk)

    assert from_db.name == "567"
