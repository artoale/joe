from joe.urls import re_path


def articles():
    pass


def drafts():
    pass


urlpatterns = [
    re_path(r"^articles/(?P<article_id>\d+)/$", articles),
    re_path(r"^drafts/(?P<article_id>\d+)/$", drafts, kwargs={"draft": True}),
]
