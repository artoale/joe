from typing import Any

from joe.conf import Settings, settings


def test_settings_read_from_module():
    # See tests/conftest.py for reference
    assert settings.ROOT_URLCONF == "example_app.urls"


def test_settings_default_to_global():
    # from joe.conf.global_settings
    assert settings.DEBUG is False


def test_ignore_lowercase(monkeypatch):
    class FakeSettingModule:
        UPPER = 1
        lower = 2

    monkeypatch.setattr("joe.conf.import_module", lambda _: FakeSettingModule())

    localsettings: Any = Settings("abc")
    assert localsettings.UPPER == 1
    assert hasattr(localsettings, "lower") is False
