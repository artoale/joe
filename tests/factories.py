from email.message import Message

from joe.request import HttpRequest


def request_factory(**kwargs):
    from io import BytesIO

    headers = kwargs.get("headers") or Message()
    return HttpRequest(
        method=kwargs.get("method", "GET"),
        path=kwargs.get("path", "/"),
        headers=headers,
        query_string=kwargs.get("query_string", ""),
        stream=kwargs.get("stream", None) or BytesIO(),
    )
