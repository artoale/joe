from joe.response import HttpResponse


def before_view(*args):
    pass


def after_view(*args):
    pass


def middleware_1(get_response):
    def instance(request):
        before_view(1)
        response = get_response(request)
        after_view(1)
        return response

    return instance


def middleware_2(get_response):
    def instance(request):
        before_view(2)
        response = get_response(request)
        after_view(2)
        return response

    return instance


def middleware_bypass(get_response):
    def instance(request):
        before_view("bypass")
        return HttpResponse(status_code=418)

    return instance


def middleware_raising(get_response):
    def instance(request):
        raise UserWarning("I'm a teapot")

    return instance


def middleware_returns_none(get_response):
    return None


class MiddlewareClass:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        response._with_class = True
        return response


class BaseMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)


class WithProcessView(BaseMiddleware):
    def process_view(self, request, view, kwargs):
        return HttpResponse(status_code=418)


class WithExceptionHandling(BaseMiddleware):
    def process_exception(self, request, exception):
        return HttpResponse(status_code=418)
