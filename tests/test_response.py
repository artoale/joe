import pytest

from joe.response import HttpResponse


def test_accepts_bytes():
    response = HttpResponse(b"A bytes string")
    assert response.content == b"A bytes string"


def test_accepts_strings():
    response = HttpResponse("A string")
    assert response.content == b"A string"


def test_content_length_set():
    response = HttpResponse(b"12345")
    assert response.headers["Content-Length"] == "5"

    response.content = "π >> τ"
    # π and τ are 2 bytes in utf-8
    assert response.headers["Content-Length"] == "8"

    # utf-32 big endian has exactly 4 bytes per code point
    response.charset = "utf-32be"

    response.content = "ABCD"
    assert response.headers["Content-Length"] == "16"


def test_defaults_to_utf8():
    response = HttpResponse("π >> τ")
    assert response.content == "π >> τ".encode("utf-8")


def test_accepts_different_charsets():
    content = "A string"
    response = HttpResponse(content, charset="ASCII")
    assert response.content == content.encode("ASCII")


def test_raises_with_unsupported_characters():
    content = "π >> τ"
    with pytest.raises(UnicodeEncodeError):
        HttpResponse(content, charset="ASCII")


def test_set_content_later_raises_if_incompatible():
    content = "π >> τ"
    response = HttpResponse(charset="ASCII")

    with pytest.raises(UnicodeEncodeError):
        response.content = content


def test_set_content_later_passes_if_compatible():
    content = "π >> τ"
    response = HttpResponse(charset="utf-8")

    response.content = content

    assert response.content == content.encode("utf-8")


def test_status_code_coherced_to_int():
    response = HttpResponse(status_code="404")  # type:ignore
    assert response.status_code == 404


def test_status_code_fails_if_not_a_number():
    with pytest.raises(ValueError):
        HttpResponse(status_code="not-a-number")  # type:ignore
